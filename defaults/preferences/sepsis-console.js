/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

pref("sepsis.console.autocomplete.activation.input", "UnambiguousShadow");
pref("sepsis.console.autocomplete.activation.tab",
     "AmbiguousPartialCompletion,UnambiguousCompletion");
pref("sepsis.console.autocomplete.activation.consecutivetab", "AmbiguousPrinting");
pref("sepsis.console.autocomplete.profile", 1);
pref("sepsis.console.integration.dominspector.overrideEval", true);
pref("sepsis.console.integration.firefox.context", 1);
pref("sepsis.console.integration.seamonkey.context", 1);
pref("sepsis.console.integration.thunderbird.context", 2);
pref("sepsis.console.draganddrop.enabled", false);
