/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * We support four "profiles" for autocomplete behavior: devtools's
 * webconsole style, bash style, vim style, and custom.
 *
 * In the webconsole, typing at the input will either show a popup
 * displaying the matches if there is more than one match, or "shadow" the
 * match in the input if there is only one match.  If the user presses Tab
 * when there is a shadowed match, the webconsole will fill in the match.
 *
 * In bash, pressing tab once will fill in the input as much as possible to
 * complete a match.  If there is more than one match, pressing tab a second
 * time will print the matches.  (For example, if you typed |"".toLoc| and
 * pressed tab, a bash-style autocomplete would find the matches
 * toLocaleLowerCase, toLocaleString, and toLocaleUpperCase.  It would fill
 * in the input as much as possible, so its value would now be
 * |"".toLocale|, and pressing tab a second time would print the matches.)
 *
 * Vim is like bash, except when the user presses tab, it will fill in the
 * input to complete the first match, even if there is more than one match.
 * Rather than printing the matches after pressing tab a second time, it
 * will cycle to the next match, if any.  Otherwise, it will simply try a
 * new autocompletion on the filled-in value.
 *
 * Notice that we can break these down into (mostly) discrete features, and
 * define profiles by feature composition.
 *
 *                                   bash      devtools      vim
 * UnambiguousCompletion        |    tab    |    tab    |    tab    |
 * AmbiguousFullCompletion      |     -     |     -     |    tab    |
 * AmbiguousPartialCompletion   |    tab    |     -     |     -     |
 * AmbiguousPrinting            | consectab |     -     |     -     |
 * AmbiguousPopup               |     -     |   input   |     -     |
 * UnambiguousShadow            |     -     |   input   |     -     |
 *
 * This allows for the fourth, custom profile type, where any of these
 * features can be activated by any event, or even multiple ones.  (NB:
 * AmbiguousFullCompletion and AmbiguousPartialCompletion are mutually
 * exclusive.  In the event both are set for the same activation type, we go
 * with the latter.  (See hacks/autoComplete.js:ACH_PrepareResponders.)
 *
 * Some terminology:
 *   Responder
 *     The values in the left column of the table above.  (UnambiguousShadow,
 *     AmbiguousPopup, et cetera.)  These are implemented in autocomplete/ and
 *     registered below.
 *  Activation type
 *    PrefManager.TAB et cetera, denoting the not-quite-event-things in the
 *    rightmost columns of the table above.
 *  consectab
 *    Consecutive tab.  That is, a tab where the previous activation type was
 *    also a tab (itself consecutive or not).
 *
 * All responders are instantiated once per window during initialization.  The
 * first parameter to a responder constructor is the AutoCompleteHack instance
 * for this window.
 *
 * Every responder must implement |handleResult|:
 *
 *   handleResult(aResult)
 *     Handle the given |ConsoleAutoCompleteResult| instance.  This will be
 *     called on the responder if the responder is named in the pref for the
 *     activation type that set off the search.  For example, if the user
 *     presses Tab and the sepsis.console.autocomplete.activation.tab pref is
 *     set to "UnambiguousCompletion", then |UnambiguousCompletionResponder|'s
 *     |handleResult| method will be called.
 *
 * @see autocomplete/ for responder implementations.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const kResponderPrefix = "chrome://sepsis-console/content/autocomplete/";
const kResponderSuffix = "Responder.js";

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("resource://gre/modules/Services.jsm");

//////////////////////////////////////////////////////////////////////////////
//// ResponderManager

/**
 * @param aOwner
 *        This will be the first (and only) argument passed to each responder
 *        constructor.
 */
function ResponderManager(aOwner)
{
  this.mOwner = aOwner;
  this.mResponders = Object.create(null);

  this._registerResponder("AmbiguousFullCompletion");
  this._registerResponder("AmbiguousPartialCompletion");
  this._registerResponder("AmbiguousPrinting");
  this._registerResponder("AmbiguousPopup");
  this._registerResponder("UnambiguousCompletion");
  this._registerResponder("UnambiguousShadow");
}

let (proto = ResponderManager.prototype) {
  proto.mOwner = null;
  proto.mResponders = null;

  proto.getResponder = function PM_GetResponder(aName)
  {
    let key = this._getKey(aName);
    return (key in this.mResponders) ?
      this.mResponders[key] :
      null;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._registerResponder = function PM_RegisterResponder(aName, aOwner)
  {
    let key = this._getKey(aName);
    if (key in this.mResponders) {
      Cu.reportError("Overwriting autocomplete responder: " + aName);
    }

    let scope = Object.create(null);
    let uri = this._getURI(aName);
    Services.scriptloader.loadSubScript(uri, scope)
    this.mResponders[key] =
      new scope[this._getConstructorName(aName)](this.mOwner);
  };

  proto._getKey = function PM_GetKey(aName)
  {
    return "@" + aName;
  };

  proto._getURI = function PM_GetURI(aName)
  {
    let baseName = aName.charAt(0).toLowerCase() + aName.substr(1);
    return kResponderPrefix + baseName + kResponderSuffix;
  };

  proto._getConstructorName = function PM_GetConstructorName(aName)
  {
    return aName + "Responder";
  };
}
