/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// ConsoleAutoCompleteSearch

/**
 * TODO:
 *   What about IdentifierStart :: \ UnicodeEscapeSequence?
 */
function ConsoleAutoCompleteSearch(aEnvironment)
{
  this.mEnvironment = aEnvironment;
}

let (proto = ConsoleAutoCompleteSearch.prototype) {
  proto.mEnvironment = null;

  /**
   * @param aString
   * @param aParam
   * @param aPreviousResult
   * @param aObserver
   */
  proto.startSearch =
    function CACS_StartSearch(aString, aParam, aPreviousResult, aObserver)
  {
    // Find the substring we need to complete on, and get the object +
    // property names that make up the thing being referenced.
    let splitPoints = CrummyTokenizer.findSplitPoints(aString).pop();
    let ids = CrummyTokenizer.splitIDs(aString, splitPoints);

    // Suppose |aString| contained a substring that we should complete on, and
    // it's
    //
    //   foo.bar["ba
    //
    // |matchOffset| should be the index of the open bracket in this example.
    // In other cases where |findSplitPoints| didn't find a suitable substring
    // for us to complete, we default to the end of the input.
    var matchOffset = aString.length;
    if (splitPoints.length) {
      matchOffset = splitPoints[splitPoints.length - 1];
    }

    // NB: |ids| may be empty.
    var lastID = ids.pop() || new idUtils.SourceThing("", false);
    var isQualified = splitPoints.length > 1;
    if (lastID.isEscaped() && !isQualified) {
      // It's just a plain (unbracketed) string literal.  We can't
      // complete that.
      return;
    }

    if (isQualified && aString[matchOffset] == "[" &&
        !lastID.getRawSource()) {
      // The search string ends with an open bracket, so all the matches need
      // to be quoted.  But |lastID.getRawSource()| is a zero-length string.
      // If we were to simply pass it to |fixUpMatches|, only the names which
      // cause |idUtils.idRequiresBracketedAccess| to return true will end up
      // being quoted.  So we change it to be an open quote.  This will force
      // |fixUpMatches| to interpret it as an unterminated string literal
      // of a zero-length known value and will give us what we want.
      lastID = new idUtils.SourceThing("\"", true);
    }

    // NB: |_x_error| is untrusted.
    var matches, _x_error;
    try {
      // For the example above, |ids| will now be a two-element array of
      // SourceThings with values |"foo"| and |"bar"|, respectively.  Resolve
      // that reference; get the foo.bar object from the environment.
      let _x_baseObj = this.mEnvironment.resolveObject(ids);

      // Obtain the prefix we need to match on ("ba" in the example above).
      let prefix = lastID.getValue(true);

      // Get the matches.
      matches = this.getPropertyNamesMatchingPrefix(_x_baseObj, prefix);

      // Fix them up to preserve any quotes or escape sequences as typed, and
      // quote any other names which require bracketed access.
      matches = this.fixUpMatches(matches, lastID);
    }
    catch (_x_ex) {
      // NB: |_x_ex| are untrusted.
      _x_error = _x_ex;
    }

    var result = new ConsoleAutoCompleteResult(aString, undefined, matches,
                                               matchOffset, isQualified,
                                               _x_error);

    aObserver.onSearchResult(this, result);
  };

  /**
   * @param a_x_Object
   *        The object whose properties we're interested in.  NB: |a_x_Object|
   *        is untrusted.
   * @param aPrefix
   *        A string which will be compared to all the known properties of
   *        |aObj|.  Any properties that begin with |aPrefix| will be
   *        returned.
   * @return
   *        An array of strings naming the properties which matched the
   *        prefix.
   */
  proto.getPropertyNamesMatchingPrefix =
    function CACS_GetPropertyNamesMatchingPrefix(a_x_Object, aPrefix)
  {
    // NB: Don't check for |!a_x_Object|, because that's a false positive if
    // |a_x_Object| is 0, false, or an empty string.
    if (a_x_Object == null) {
      return [];
    }

    var names = [];
    // |Object.getPrototypeOf| throws a TypeError if its argument is a
    // primitive (such as a string).  So we need to coerce (non-null,
    // non-undefined) primitives into their Object forms before trying to work
    // with them.
    //
    // NB: |_x_obj| is untrusted.
    let _x_obj = Object(a_x_Object);
    while (_x_obj) {
      // Even if |_x_obj| is a proxy with a trap on |getOwnPropertyNames|,
      // |ownNames| is an array (the standard kind) of primitive strings,
      // unless the trap throws.
      let ownNames;
      try {
        ownNames = Object.getOwnPropertyNames(_x_obj);
      }
      catch (_x_ex) {
        // NB: |_x_ex| is untrusted.
        // XXX l10n
        let objectQualifier = (_x_obj === a_x_Object) ?
          "" :
          "prototype chain ";
        this.mEnvironment.consoleWriter.print(
          "Encountered error (", _x_ex,
          ") when trying to get properties of ", objectQualifier, "object ",
          _x_obj, "."
        );

        ownNames = [];
      }
      for (let i = 0, n = ownNames.length; i < n; ++i) {
        let currentName = ownNames[i];

        // Make sure the name matches the given prefix, and don't include
        // array or string indexes as matches; they're just not interesting.
        if (currentName.indexOf(aPrefix) == 0 &&
            (isNaN(currentName) || currentName == "")) {
          names.push(currentName);
        }
      }

      _x_obj = Object.getPrototypeOf(_x_obj);
    }

    // Sort the names and prune duplicates.
    names.sort();
    var matches = [];
    for (let lastName, i = 0, n = names.length; i < n; ++i) {
      let currentName = names[i];
      if (currentName != lastName) {
        matches.push(currentName);
      }

      lastName = currentName;
    }

    return matches;
  };

  /**
   * Given a list of property names, fix them up to fit the format necessary
   * to work as acceptable completions.
   *
   * For example, if a matched name contains special characters, it will need
   * to be quoted (so that it may be accessed using bracket notation).
   *
   * Similarly, if the user began typing the property name using bracket
   * notation, all matches will need to be quoted (using the same type of
   * quotes initially typed in by the user.)  If escape sequences were used in
   * a bracketed access, the matches should include those same sequences in
   * the positions that they exist in the string partially typed by the user.
   *
   * @param aMatches
   *        An array of matching property names generated by this
   *        auto-complete.  (NB: At this point, not all names in this list are
   *        guaranteed to match.  The user may have begun typing [in a
   *        bracketed property access] an escape sequence that, although
   *        unterminated, is still specific enough that some names can be
   *        ruled out.  We'll take care of this below.  See
   *        |_getFullSequences|.)
   * @param aInput
   *        A SourceThing corresponding to the search substring describing
   *        exactly how the user had begun to type the property name.  E.g.,
   *        the |prop| in |.prop|, the |"prop| in |["prop|, et cetera.
   */
  proto.fixUpMatches = function CACS_FixUpMatches(aMatches, aInput)
  {
    let parser, quoteType;
    if (aInput.isEscaped()) {
      quoteType = aInput.getRawSource().charAt(0);
      parser = aInput.getParser();
    }

    let result = [];
    for (let i = 0, n = aMatches.length; i < n; ++i) {
      let match = aMatches[i];

      if ((parser && quoteType) || idUtils.idRequiresBracketedAccess(match)) {
        let fullSequences;
        if (parser) {
          try {
            fullSequences =
              this._getFullSequences(match, parser.escapeSequences);
          }
          catch (ex) {
            // |_getFullSequences| couldn't reconcile the current name with
            // the partial escape sequences; this is a non-match.  Discard it.
            continue;
          }
        }

        match = StringParser.uneval(match, quoteType, fullSequences)
      }

      result.push(match);
    }

    return result;
  };

  /**
   * Given a string and an array of escape sequences, where the last sequence
   * may be unterminated, return an array of sequences such that for every
   * element in the returned array, the escape sequence is a valid way to
   * encode the corresponding character in the given string.
   *
   * @param aString
   *        The reference string.
   * @param aEscapeSequences
   * @return
   *        An array of escape sequences, matching the format of
   *        |StringParser.escapeSequences|, where the last element may differ
   *        from |aEscapeSequences| after being made to match the
   *        corresponding character in |aString|.
   * @throws
   *        Error if the last sequence could not be completed to match
   *        |aString|.
   * @see guessSequence
   */
  proto._getFullSequences =
    function CACS_GetFullSequences(aString, aEscapeSequences)
  {
    if (!aEscapeSequences || !aEscapeSequences.length) {
      return aEscapeSequences;
    }

    // We don't need to worry about checking that the last sequence is, in
    // fact, unterminated; for sequences which aren't unterminated,
    // |guessSequence| behaves idempotent-ish-ly.
    var last = aEscapeSequences[aEscapeSequences.length - 1];
    var rest = aEscapeSequences.slice(0, aEscapeSequences.length - 1);
    var c = aString[last.index];

    return rest.concat([{
      index: last.index,
      escapeSequence: this.guessSequence(last.escapeSequence, c)
    }]);
  };

  /**
   * Given a partial escape sequence and some character, try to complete the
   * sequence such that it's a valid way to encode the given character if the
   * sequence were parsed as a string.
   *
   * We prefer simple, superfluous escape sequences for printable ASCII when
   * possible (i.e., |"\a"| for |"a"|, but not |"\n"| for |"n"| since that
   * sequence is meaningful as a newline), then Unicode escape sequences,
   * then hex escape sequences, and finally octal.
   *
   * E.g.,
   *
   *   guessSequence("\\", "m") => "\\m"
   *   guessSequence("\\u00", "m") => "\\u006d"
   *   guessSequence("\\x", "m") => "\\x6d"
   *   guessSequence("\\1", "m") => "\\155"
   *
   * @param aUnterminatedSequence
   *        The partial escape sequence (with or without leading backslash).
   * @param aCharacter
   *        The character that the return value should encode.
   * @return
   *        The escape sequence.  The return value will include a leading
   *        backslash if |aUnterminatedSequence| has one.
   * @throws
   *        Error if we could not generate a complete sequence to match
   *        |aCharacter|, using |aUnterminatedSequence| as a starting point.
   */
  proto.guessSequence =
    function CACS_GuessSequence(aUnterminatedSequence, aCharacter)
  {
    var sequence = aUnterminatedSequence;
    var sequenceMarker = aUnterminatedSequence.charAt(0);
    if (sequenceMarker == "\\") {
      sequence = sequence.substring(1);
    }
    else {
      sequenceMarker = "";
    }

    // XXX Case sensitivity.
    var result, charCode = aCharacter.charCodeAt(0);
    switch (sequence.charAt(0)) {
      case "u":
        let unicodeSequence = charCode.toString(16);

        let pad = "";
        for (let i = 0, n = 4 - unicodeSequence.length; i < n; ++i) {
          pad += "0";
        }

        result = "u" + pad + unicodeSequence;
      break;
      case "x":
        let hexSequence = charCode.toString(16);
        result = (hexSequence.length < 2) ?
          "x0" + hexSequence :
          "x" + hexSequence;
      break;
      case "0":
      case "1":
      case "2":
      case "3":
      case "4":
      case "5":
      case "6":
      case "7":
        if (charCode < 256) {
          let octalSequence = charCode.toString(8);
          if (octalSequence.indexOf(sequence) != 0 &&
              octalSequence.length < 3) {
            octalSequence = "0" + octalSequence;

            if (octalSequence.indexOf(sequence) != 0 &&
                octalSequence.length < 3) {
              octalSequence = "0" + octalSequence;
            }
          }

          result = octalSequence;
        }
      break;
      default:
        // First we check for special characters.
        for (let prop in StringParser.specialCharacters) {
          if (aCharacter == StringParser.specialCharacters[prop]) {
            result = prop;
          }
        }
        if (result !== undefined) {
          break;
        }

        if ((aCharacter in StringParser.specialCharacters) ||
            (!sequence.length && (charCode < 32 || charCode > 126))) {
          // Maximum recursion depth: 2.
          return this.guessSequence(sequenceMarker + "u", aCharacter);
        }
        else {
          result = aCharacter;
          // If this isn't right, we'll catch it with |indexOf| below.
        }
      break;
    }

    result = sequenceMarker + result;
    if (result.indexOf(aUnterminatedSequence) == 0) {
      return result;
    }

    throw Error("no escape sequence beginning with " +
                "\"" + sequenceMarker + sequence.quote().substring(1) +
                " can be made to match character " + aCharacter.quote());
  };
}

//////////////////////////////////////////////////////////////////////////////
//// ConsoleAutoCompleteResult

/**
 * NB: |ConsoleAutoCompleteResult|s' |errorDescription| is untrusted.
 *
 * @param aString
 * @param aStatus
 * @param aCompletions
 * @param aMatchOffset
 * @param aIsQualified
 * @param a_x_Error
 *        NB: |a_x_Error| is untrusted.
 */
function ConsoleAutoCompleteResult(aString, aStatus, aCompletions,
                                   aMatchOffset, aIsQualified, a_x_Error)
{
  this.searchString = aString;

  // nsIAutoCompleteResult's attribute names are stupid.
  this.searchResult = aStatus;

  this.mCompletions = aCompletions;

  this.matchOffset = aMatchOffset;

  this.isQualified = aIsQualified;

  if (a_x_Error !== undefined) {
    // Bah.  These interfaces really do need to be redesigned.
    this.errorDescription = a_x_Error;
  }
}

let (proto = ConsoleAutoCompleteResult.prototype) {
  proto.mCompletions = null;

  proto.errorDescription = null;
  proto.isQualified = null;
  proto.matchOffset = null;
  proto.searchResult = null;
  proto.searchString = null;

  proto.__defineGetter__("matchCount", function CACR_MatchCount_Get()
  {
    return this.mCompletions.length;
  });

  proto.getValueAt = function CACR_GetValueAt(aIndex)
  {
    return this.mCompletions[aIndex];
  };
}
