/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// AmbiguousPrintingResponder

/**
 * Responder for printing all matches to the console when more than one match
 * is found.  For example, if the input is
 *
 *   Math.L
 *
 * ... when |handleResult| is called, it will print
 *
 *   Math.LN10
 *   Math.LN2
 *   Math.LOG10E
 *   Math.LOG2E
 *
 * @see autocomplete/responderManager.js
 * @constructor
 */
function AmbiguousPrintingResponder(aAutoCompleteHack)
{
  this.mAutoCompleteHack = aAutoCompleteHack;
}

let (proto = AmbiguousPrintingResponder.prototype) {
  proto.mAutoCompleteHack = null;

  proto.handleResult = function APR_HandleResult(aResult)
  {
    if (aResult.matchCount < 2) {
      return;
    }

    // XXX This probably shouldn't live here, either.  This should be in a
    // purpose-specific method of the console writer that we make a simple
    // call to, like the other responders do with |completeInline|.
    this.mAutoCompleteHack.consoleWriter.print(aResult.searchString);
    for (let i = 0, n = aResult.matchCount; i < n; ++i) {
      this.mAutoCompleteHack.consoleWriter.print(
        "  " + aResult.getValueAt(i)
      );
    }
  };
}
