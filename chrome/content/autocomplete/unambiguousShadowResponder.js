/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// UnambiguousShadowResponder

/**
 * When only one match is found, this responder's |handleEvent| method will
 * "shadow" it in the input.  That is, the rest of the match will be filled in
 * ahead of the cursor and will be dimmer.
 *
 * @see autocomplete/responderManager.js
 * @constructor
 */
function UnambiguousShadowResponder(aAutoCompleteHack)
{
  this.mAutoCompleteHack = aAutoCompleteHack;
}

let (proto = UnambiguousShadowResponder.prototype) {
  proto.mAutoCompleteHack = null;

  proto.handleResult = function USR_HandleResult(aResult)
  {
    if (aResult.matchCount != 1) {
      return;
    }

    this.mAutoCompleteHack.completeInline(aResult.getValueAt(0), aResult,
                                          undefined, true);
  };
}
