/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// UnambiguousCompletionResponder

/**
 * Responder for completing the input when exactly one match is found.  For
 * example, if the input is
 *
 *   Object.getPr
 *
 * ... and the "getPrototypeOf" method is the only match (assuming a typical
 * execution context, this should be the case), when |handleResult| is called
 * it will complete the input to
 *
 *   Object.getPrototypeOf
 *
 * @see autocomplete/responderManager.js
 * @constructor
 */
function UnambiguousCompletionResponder(aAutoCompleteHack)
{
  this.mAutoCompleteHack = aAutoCompleteHack;
}

let (proto = UnambiguousCompletionResponder.prototype) {
  proto.mAutoCompleteHack = null;

  proto.handleResult = function UCR_HandleResult(aResult)
  {
    if (aResult.matchCount != 1) {
      return;
    }

    this.mAutoCompleteHack.completeInline(aResult.getValueAt(0), aResult);
  };
}
