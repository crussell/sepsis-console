/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// AmbiguousFullCompletionResponder

/**
 * When more than one match is found, this responder's |handleResult| method
 * will complete the input to the first match (in lexicographic order).
 * Calling |handleResult| again will cycle to the next match, and so on.  If
 * the entire list has been traversed, the next call to |handleResult| will
 * reset the input to the original string (read: no match), and the next call
 * will start back and the first match.
 *
 * For example, if the input is
 *
 *   Number.M
 *
 * ... |handleResult| will complete it to
 *
 *   Number.MAX_VALUE
 *
 * The next call will complete it to
 *
 *   Number.MIN_VALUE
 *
 * The next call will reset it back to no match:
 *
 *   Number.M
 * 
 * XXX Unimplemented.
 *
 * @see autocomplete/responderManager.js
 * @constructor
 */
function AmbiguousFullCompletionResponder(aAutoCompleteHack)
{
  this.mAutoCompleteHack = aAutoCompleteHack;
}

let (proto = AmbiguousFullCompletionResponder.prototype) {
  proto.mAutoCompleteHack = null;

  proto.handleResult = function AFCR_HandleResult(aResult)
  {
    if (aResult.matchCount < 2) {
      return;
    }

    // XXX
    Components.utils.reportError("AmbiguousFullCompletion not yet supported");
  };
}
