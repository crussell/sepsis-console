/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// AmbiguousPopupResponder

/**
 * When more than one match is found, this responder will show a popup listing
 * all matches.  Selecting a match using the arrow keys will shadow it in the
 * input, and pressing Enter on a selected match or clicking a match will
 * complete it in the input and close the popup.
 *
 * XXX This responder might already be doing too much.  This isn't
 * AmbiguousPopupWhereSelectionShadowsResponder.  It's feature parity with the
 * Web Console, though.  It would be nice if there were a clean way to break
 * this out and handle it elsewhere. --crussell
 *
 * @see autocomplete/responderManager.js
 * @constructor
 */
function AmbiguousPopupResponder(aAutoCompleteHack)
{
  this.mAutoCompleteHack = aAutoCompleteHack;

  this.mInput = this.mAutoCompleteHack.input;
  this.mPopup = this.mAutoCompleteHack.popup;

  this.mTree = document.getElementById("bxAutoCompleteResultsTree");

  // XXX Do we need to handle removing these?  Maybe if prefs don't have
  // this responder enabled.
  this.mPopup.addEventListener("popupshowing", this, false);
  this.mPopup.addEventListener("popuphiding", this, false);

  this.mTree.addEventListener("select", this, false);
  this.mTree.addEventListener("click", this, false);
}

let (proto = AmbiguousPopupResponder.prototype) {
  proto.mAutoCompleteHack = null;
  proto.mInput = null;
  proto.mPopup = null;
  proto.mTree = null;
  proto.mTreeView = null;

  proto.handleResult = function APpR_HandleResult(aResult)
  {
    // XXX We check for the match offset being the end of the search string to
    // prevent showing a popup showing everything in scope.  If we didn't,
    // typing, e.g., "foo;" will show the popup, which can be annoying when
    // we're preffed to be activated by input.  This does come at the expense
    // of perhaps not behaving as expecting when we're preffed to respond to
    // tab, in which case we'll also show no popup...
    if (aResult.matchCount < 2 ||
        aResult.matchOffset == aResult.searchString.length) {
      this.mPopup.hidePopup();
      return;
    }

    this.mTreeView = new AutoCompleteTreeView(aResult);
    this.mTree.view = this.mTreeView;
    this.mPopupHack =
      new AutoCompletePopupSizeHack(this.mPopup, this.mTree, aResult);

    // XXX Check if the popup is already open here, to prevent it from jumping
    // around.
    let xOffset = this._computePopupOffset(aResult);
    this.mPopup.openPopup(this.mInput, "after_start", xOffset, 0, false,
                          false, null);
  };

  proto.handleEvent = function APpR_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "popupshowing":
        this._onPopupShowing(aEvent);
      break;
      case "popuphiding":
        this._onPopupHiding(aEvent);
      break;
      case "keypress":
        this._onKeyPress(aEvent);
      break;
      case "click":
        this._onClick(aEvent);
      break;
      case "select":
        this._onSelect(aEvent);
      break;
    }
  };

  proto._onPopupShowing = function APpR_OnPopupShowing(aEvent)
  {
    // XXX |historyNavSuspended| and |enterSuspended| start making this pretty
    // hacky, too.  (Worse still, it's spreading outside, into the console
    // input type binding.)  Maybe take another look at the autocomplete
    // interfaces proper and cleaning them up.  That should have broad
    // benefits (for toolkit) as well.
    this.mInput.historyNavSuspended = true;
    this.mInput.enterSuspended = true;
    this.mInput.addEventListener("keypress", this);
  };

  proto._onPopupHiding = function APpR_OnPopupHiding(aEvent)
  {
    this.mInput.enterSuspended = false;
    this.mInput.historyNavSuspended = false;
    this.mInput.removeEventListener("keypress", this);
  };

  proto._onKeyPress = function APpR_OnKeyPress(aEvent)
  {
    var idx, selection = this.mTreeView.selection;
    switch (aEvent.keyCode) {
      case aEvent.DOM_VK_DOWN:
      case aEvent.DOM_VK_UP:
        idx = (aEvent.keyCode == aEvent.DOM_VK_UP) ?
          selection.currentIndex - 1 :
          selection.currentIndex + 1;
        if (idx < -1) {
          idx = this.mTreeView.rowCount - 1;
        }
        else if (idx == this.mTreeView.rowCount) {
          idx = -1;
        }

        // XXX Pfft. I'd expect calling |ensureRowIsVisible| with a negative
        // index to clamp it to zero, but it just shows n blank rows before
        // row 0, where n = |Math.abs(index)|.  Bug? --crussell
        if (idx < 0) {
          this.mTree.treeBoxObject.ensureRowIsVisible(0);
        }
        else {
          this.mTree.treeBoxObject.ensureRowIsVisible(idx);
        }
        selection.select(idx);

        this.mInput.enterSuspended = idx >= 0;
      break;
      case aEvent.DOM_VK_RETURN:
        if (selection.currentIndex < 0) {
          return;
        }

        this._completeSelectedMatch();
      break;
      default:
        return;
      break;
    }

    aEvent.preventDefault();
  };

  proto._onClick = function APpR_OnClick(aEvent)
  {
    this._completeSelectedMatch();
  };

  proto._onSelect = function APpR_OnSelect(aEvent)
  {
    // Shadow the selected match.
    this._completeSelectedMatch(true);
  };

  /**
   * @param aShadow [optional]
   *        Whether or not the completed match should be shadowed.  Default is
   *        false.
   */
  proto._completeSelectedMatch = function APpR_CompleteSelectedMatch(aShadow)
  {
    this.mAutoCompleteHack.enoughWithTheShadow();

    if (this.mTreeView.selection.currentIndex < 0) {
      return;
    }

    var result = this.mTreeView.result;
    var name = result.getValueAt(this.mTreeView.selection.currentIndex);
    this.mAutoCompleteHack.completeInline(name, result, undefined, aShadow);

    if (!aShadow) {
      this.mPopup.hidePopup();
    }
  };

  proto._computePopupOffset = function APpR_ComputePopupOffset(aResult)
  {
    // The popup should appear just above/below the result's match prefix.
    // We'll use the metrics from a client rect to figure out where that is.
    // We can easily get a usable rect from a selection range.
    //
    // To position the range, we use its |setStart| and |setEnd| methods.  We
    // need a container to pass as the first argument to each, as the second
    // argument is taken to be the offset into that container.  The way the
    // editor is implemented, it contains a native anonymous text node in a
    // native anonymous div.  The text node's contents comprise the input's
    // value. We want the text node to be the container for our range.
    //
    // This can give us either the div or the text node, so we'll have check
    // it to ensure we get the latter.
    let container =
      this.mInput.editor.selection.getRangeAt(0).commonAncestorContainer;
    if (container.nodeType != container.TEXT_NODE) {
      // It was the div.  This should do it:
      container = container.firstChild;
    }
    // assert(container.nodeType == container.TEXT_NODE);

    // All right.  It would be nice if we could just collapse the range at
    // |aResult.matchOffset| and use the client rect's left or right, but
    // ranges' |getBoundingClientRect| only gives use a useful rect for
    // non-collapsed ranges; for collapsed ranges, left, right, top, and
    // bottom are all zero.  So we need to make sure it's non-collapsed before
    // requesting a rect.  That means the range's start offset and end offset
    // have to be non-equal, but still such that we can get a useful value
    // from the client rect metrics.
    //
    // We can achieve that by trying to add some of the adjacent text to the
    // range.
    //
    // There are three cases:
    // - The input has some text to the right of the match offset.
    // - There's some text to the left of the match offset, but none to the
    //   right.
    // - There's no text to the left or the right of the match offset.
    let x, range = this.mInput.ownerDocument.createRange();
    // Remember that member accesses will be qualified by a dot or an open
    // bracket.  We want the popup position to account for that, too.
    let offset = (aResult.isQualified) ?
      aResult.matchOffset + 1 :
      aResult.matchOffset;
    if (offset < this.mInput.value.length) {
      // Case 1.  Put the character to the right in the range, and use its
      // left boundary.
      range.setStart(container, offset);
      range.setEnd(container, offset + 1);
      x = range.getBoundingClientRect().left;
    }
    else if (offset > 0) {
      // Case 2.  Put the character to the left in the range, and use its
      // right boundary.
      range.setStart(container, offset - 1);
      range.setEnd(container, offset);
      x = range.getBoundingClientRect().right;
    }
    else {
      // Case 3.  The input's empty.  Forget the range, and just use the left
      // boundary of the div.
      x = container.parentNode.getBoundingClientRect().left;
    }

    // Remember, this offset needs to be relative to the popup's anchor
    // container, which is the input.
    return x - this.mInput.getBoundingClientRect().left;
  };
}

/**
 * @constructor
 */
function AutoCompleteTreeView(aResult)
{
  this.result = aResult;
}

let (proto = AutoCompleteTreeView.prototype) {
  let stub = function ACTV_Stub() { };
  let falseStub = function ACTV_FalseStub() { return false; };
  let getPropertiesStub = function ACTV_GetPropertiesStub()
  {
    return "";
  };

  proto.result = null;

  proto.selection = null;

  proto.__defineGetter__("rowCount", function ACTV_RowCount_Get()
  {
    return this.result.matchCount;
  });

  proto.getCellText = function ACTV_GetCellText(aIndex, aColumn)
  {
    return this.result.getValueAt(aIndex);
  };

  proto.hasNextSibling = function ACTV_HasNextSibling(aIndex, aAfterIndex)
  {
    return aAfterIndex < this.rowCount - 1;
  };

  proto.getParentIndex = function ACTV_GetParentIndex()
  {
    return -1;
  };

  proto.getLevel = function ACTV_GetLevel()
  {
    return 0;
  };

  proto.getImageSrc = function ACTV_GetImageSrc()
  {
    return "";
  };

  proto.cycleHeader =
  proto.cycleCell =
  proto.drop =
  proto.getCellValue =
  proto.performAction =
  proto.performActionOnRow =
  proto.performActionOnCell =
  proto.selectionChanged =
  proto.setCellValue =
  proto.setCellText =
  proto.setTree =
  proto.toggleOpenState = stub;

  proto.canDrop =
  proto.isContainer =
  proto.isContainerEmpty =
  proto.isContainerOpen =
  proto.isEditable =
  proto.isSelectable =
  proto.isSeparator =
  proto.isSorted = falseStub;

  proto.getColumnProperties =
  proto.getRowProperties =
  proto.getCellProperties = getPropertiesStub;
}
