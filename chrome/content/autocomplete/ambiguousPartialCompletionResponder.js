/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// AmbiguousPartialCompletionResponder

/**
 * Responder for completing the input when more than one match is found for
 * the input.  For example, if the input is
 *
 *   Components.classes["@moz
 *
 * ... there will be many matches.  When |handleResult| is called, it will
 * complete the input as much as it can, to
 *
 *   Components.classes["@mozilla.org/
 *
 * @see autocomplete/responderManager.js
 * @constructor
 */
function AmbiguousPartialCompletionResponder(aAutoCompleteHack)
{
  this.mAutoCompleteHack = aAutoCompleteHack;
}

let (proto = AmbiguousPartialCompletionResponder.prototype) {
  proto.mAutoCompleteHack = null;

  proto.handleResult = function APCR_HandleResult(aResult)
  {
    if (aResult.matchCount < 2) {
      return;
    }

    // Find the longest common prefix.
    var partial;
    for (let k = 0; partial == undefined; ++k) {
      let compareTo = aResult.getValueAt(0).charAt(k);

      for (let i = 1, n = aResult.matchCount; i < n; ++i) {
        let c = aResult.getValueAt(i).charAt(k);
        if (!c || c != compareTo) {
          partial = aResult.getValueAt(i).substring(0, k);
          break;
        }
      }
    }

    this.mAutoCompleteHack.completeInline(partial, aResult, true);
  };
}
