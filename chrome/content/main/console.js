/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * A Mozilla toolkit-based console implementation.
 *
 * TODO:
 *   Perform version detection and allow some control from the UI over what
 *   gets passed as the version string to the sandbox eval.
 *   Increment a running line count for sandbox evals and use it as the line
 *   number.
 *   We should be able attach to more than just document windows.  What about
 *   JSMs and sandboxes?
 */

//////////////////////////////////////////////////////////////////////////////
//// Global Variables

var app;

//////////////////////////////////////////////////////////////////////////////
//// Global Constants

const kXULNSURI =
  "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";

//////////////////////////////////////////////////////////////////////////////
//// Imported Symbols

Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import("chrome://sepsis-console/content/utils/prefManager.jsm");
Cu.import("chrome://sepsis-console/content/utils/stringParser.jsm");

//////////////////////////////////////////////////////////////////////////////

window.addEventListener("load", initializeConsoleApp, false);

function initializeConsoleApp()
{
  app = new ConsoleApp();

  var obs =
    Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
  obs.notifyObservers(window, "sepsis-console-instantiated", null);
}

//////////////////////////////////////////////////////////////////////////////
//// ConsoleApp

/**
 * Class responsible for driving the UI and responding to UI events.
 */
function ConsoleApp()
{
  this.mInput = document.getElementById("txConsoleInput");
  this.mScrollback = document.getElementById("bxScrollback");
  this.mDocItemsTooltip = document.getElementById("ppDocItemsTooltip");
  this.mContentNoneItem = document.getElementById("mnAttachToContentNone");
  this.mChromeNoneItem = document.getElementById("mnAttachToChromeNone");

  this.mPrefManager = PrefManager;

  this.mStringBundle = document.getElementById("consoleStringBundle");

  this.consoleWriter =
    new ConsoleWriter(this.mScrollback, this.mStringBundle);

  let (context = null) {
    if ("arguments" in window && window.arguments.length) {
      context = window.arguments[0];
    }
    this.mConsoleEnvironment =
      new ConsoleEnvironment(context, this.consoleWriter);
  }

  this.mDropResponder =
    new DropResponder(this.mConsoleEnvironment, this.consoleWriter);
  this.mScrollbackDragWatcher =
    new ScrollbackDragWatcher(this.consoleWriter, this.mDropResponder);

  // Make sure the input resizer hack is initialized after the autocomplete
  // hack, so that the resizer listeners fire after any completeInline calls.
  this.mAutoCompleteHack =
    new AutoCompleteHack(this.mInput, this.mConsoleEnvironment,
                         this.consoleWriter, this.mPrefManager);
  this.mInputResizer = new ConsoleInputResizer(this.mInput, this.mScrollback);

  // Make sure that we don't attach our command listener until after the
  // auto-complete hack has been instantiated, so that its listener fires
  // before ours and has a chance to fix up the input value if it wants before
  // we eval it.  When AutoCompleteHack goes away, we should be able to just
  // move this into the oncommand attribute in the XUL.
  this.mInput.addEventListener("command", this, false);
  this.mScrollback.contentDocument.addEventListener("click", this, false);

  this.updateEditMenuItems();
}

let (proto = ConsoleApp.prototype) {
  proto.mChromeNoneItem = null;
  proto.mContentNoneItem = null;
  proto.mConsoleEnvironment = null;
  proto.mDocItemsTooltip = null;
  proto.mInput = null;
  proto.mInputResizer = null;
  proto.mPrefManager = null;
  proto.mScrollback = null;
  proto.mStringBundle = null;

  proto.__defineGetter__("consoleEnvironment",
    function CA_ConsoleEnvironment_Get()
  {
    return this.mConsoleEnvironment;
  });

  proto.consoleWriter = null;

  proto.newWindow = function CA_NewWindow()
  {
    // TODO Pref for whether this is same-context or empty sandbox? --crussell
    sepsis.openConsole(this.mConsoleEnvironment.context);
  };

  proto.closeWindow = function CA_CloseWindow()
  {
    window.close();
  };

  proto.onUnload = function CA_OnUnload()
  {
    if (this.mConsoleEnvironment) {
      this.mConsoleEnvironment.destroy();
    }
  };

  proto.quit = function CA_Quit()
  {
    var appStartup =
      Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup);
    appStartup.quit(Ci.nsIAppStartup.eAttemptQuit);
  };

  proto.onAttachMenuShown = function CA_OnAttachMenuShown(aPopup)
  {
    // Don't let us to attach to our own window.
    for (let item = aPopup.firstChild; item; item = item.nextSibling) {
      if (item.targetDocument == document ||
          item.targetDocument == this.mScrollback.contentDocument) {
        item.setAttribute("disabled", true);
      }
    }
  };

  /**
   * Command handler for the "Attach [...]" menus.
   *
   * @param aItem
   *        The menuitem selected.  It must have a |targetDocument| property.
   */
  proto.onSelectDocumentItem = function CA_OnSelectDocumentItem(aItem)
  {
    this.mConsoleEnvironment.context =
      Cu.getGlobalForObject(aItem.targetDocument);
  };

  proto.onEditPopupShowing = function CA_OnEditPopupShowing()
  {
    var multiLineModeItem = document.getElementById("mnMultiLineMode");
    if (this.mInput.multiLineMode) {
      multiLineModeItem.setAttribute("checked", "true");
    }
    else {
      multiLineModeItem.removeAttribute("checked");
    }
  };

  proto.doCommand = function CA_DoCommand(aCommandName)
  {
    var controller =
      document.commandDispatcher.getControllerForCommand(aCommandName);

    if (controller && controller.isCommandEnabled(aCommandName)) {
      controller.doCommand(aCommandName);
    }
  };

  /**
   * Update a command's disabled state.
   *
   * @param aCommandName
   *        The name of the command.  This should be the element ID of a
   *        XUL command element.
   */
  proto.updateCommand = function CA_UpdateCommand(aCommandName)
  {
    var command = document.getElementById(aCommandName);
    if (command) {
      let controller =
        document.commandDispatcher.getControllerForCommand(aCommandName);

      if (controller && controller.isCommandEnabled(aCommandName)) {
        command.removeAttribute("disabled");
      }
      else {
        command.setAttribute("disabled", "true");
      }
    }
  };

  proto.updateEditMenuItems = function CA_UpdateGlobalEditMenuItems()
  {
    this.updateUndoEditMenuItems();

    this.updateCommand("cmd_cut");
    this.updateCommand("cmd_copy");

    this.updatePasteEditMenuItems();

    this.updateCommand("cmd_delete");
    this.updateCommand("cmd_selectAll");
  };

  proto.updateUndoEditMenuItems = function CA_UpdateUndoEditMenuItems()
  {
    this.updateCommand("cmd_undo");
    this.updateCommand("cmd_redo");
  };

  proto.updatePasteEditMenuItems = function CA_UpdatePasteEditMenuItems()
  {
    this.updateCommand("cmd_paste");
  };

  proto.toggleMultiLineMode = function CA_ToggleMultiLineMode(aEvent)
  {
    // If the input had focus, its handler will execute and prevent ours from
    // firing with preventDefault().  There's no need to handle the opposite
    // handler sequence, because bindings' handlers always execute first.
    // XXX That might be a lie... --crussell
    // XXX Explain this better; case breakdown
    this.mInput.toggleMultiLineMode();
  };

  proto.handleEvent = function CA_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "command":
        this._onCommand(aEvent);
      break;
      case "click":
        this._onClick(aEvent);
      break;
      default:
        Cu.reportError("wat.  Event: " + aEvent.type);
      break;
    }
  };

  proto._onCommand = function CA_OnCommand(aEvent)
  {
    var clearInput = this.mConsoleEnvironment.evalInput(this.mInput.value);
    // Don't clear the input if there was a problem evaluating its contents.
    // TODO Pref this behavior? --crussell
    if (!clearInput) {
      aEvent.preventDefault();
    }
  };

  proto._onClick = function CA_OnClick(aEvent)
  {
    if (!aEvent.target.hasAttribute("filename")) {
      return;
    }

    let line = aEvent.target.getAttribute("linenumber");
    let file = aEvent.target.getAttribute("filename");
    gViewSourceUtils.viewSource(file, null, null, line);

    aEvent.preventDefault();
  };
}
