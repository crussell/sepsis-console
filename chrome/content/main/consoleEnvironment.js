/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Imported Symbols

Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import("chrome://sepsis-console/content/utils/jsValueToString.jsm");

//////////////////////////////////////////////////////////////////////////////
//// ConsoleEnvironment

/**
 * Class responsible for evaling input and otherwise managing the execution
 * environment.
 *
 * NB: |ConsoleEnvironment|s' |context| property is untrusted.
 *
 * @param a_x_Context
 *        The context the input should execute in.  Used as the sandbox global
 *        for evaled input.
 *        NB: |a_x_Context| is untrusted.
 * @param aConsoleWriter
 *        The object implementing the console writer interface to echo the
 *        input and write eval results to the scrollback.
 */
function ConsoleEnvironment(a_x_Context, aConsoleWriter)
{
  this.mIsObserving = false;
  this.context = a_x_Context;
  this.consoleWriter = aConsoleWriter;
}

let (proto = ConsoleEnvironment.prototype) {
  proto.mIsObserving = null;
  proto.mSandbox = null;

  proto._x_context = null;

  proto.QueryInterface = XPCOMUtils.generateQI([ Ci.nsIObserver ]);

  proto.consoleWriter = null;

  proto.__defineGetter__("context", function CE_Context_Get()
  {
    return this._x_context;
  });

  proto.__defineSetter__("context", function CE_Context_Set(a_x_Context)
  {
    // NB: |a_x_Context| is untrusted.
    this._x_context = a_x_Context;

    if (a_x_Context) {
      this.mSandbox =
        Cu.Sandbox(a_x_Context,
                   { sandboxPrototype: a_x_Context, wantXrays: false });
    }
    else {
      // Otherwise, don't attach to anything.  Just give them an empty sandbox
      // to play around in.
      let doc = document.implementation.createHTMLDocument("");
      // XXX Do we even need wantXrays here?
      this.mSandbox = Cu.Sandbox(doc.nodePrincipal, { wantXrays: false });
      // It used to be at this point that we'd inject a |console| object into
      // the sandbox, but that broke because the awesome things that awesome
      // devtools awesome-does.  We do without for now because I don't have
      // the budget to fix it at this time.  Fuck.  --crussell
    }

    if (a_x_Context instanceof Ci.nsIDOMWindow && !this.mIsObserving) {
      this.mIsObserving = true;
      Services.obs.addObserver(this, "console-api-log-event", false);
    }
  });

  proto.__defineGetter__("sandbox", function CE_Sandbox_Get()
  {
    return this.mSandbox;
  });

  proto.destroy = function CE_Destroy()
  {
    if (this.mIsObserving) {
      Services.obs.removeObserver(this, "console-api-log-event");
    }
  };

  /**
   * Evaluate the given string in the current context.
   *
   * @param aInputStr
   *        The string to eval.
   * @param aResult [optional]
   *        Outparam for the result of the eval.  Additionally, the
   *        |_x_exception| property of the given object will be set to any
   *        exception encountered during eval.
   *        NB: |aResult|'s |_x_value| and |_x_exception| properties are
   *        untrusted.
   * @param aWrite [optional]
   *        False if the eval result shouldn't be written to the output.
   *        Defaults to true.
   * @return
   *        False if the eval failed in some way, true otherwise.
   */
  proto.evalInput = function CE_EvalInput(aInputStr, aResult, aWrite)
  {
    var result = aResult || {};
    var write = (aWrite === undefined) ?
      true :
      !!aWrite;

    if (write) {
      this.consoleWriter.writeInput(aInputStr);
    }

    try {
      // NB: |result._x_value| is untrusted.
      result._x_value = Cu.evalInSandbox(aInputStr, this.mSandbox, "ECMAv5",
                                         "sepsis-console", 0);
      if (result._x_value !== undefined && write) {
        // XXX In the event jsValueToString encounters an error trying to get
        // an appropriate string representation, it would be nice we if could
        // be signalled about it, so we could print something helpful along
        // with the fallback return value it provides.
        this.consoleWriter.writeValue(result._x_value);
      }
    }
    catch (_x_ex) {
      // NB: |result._x_exception| and |_x_ex| are untrusted.
      result._x_exception = _x_ex;

      if (write) {
        this.consoleWriter.writeValue(_x_ex);
      }

      return false;
    }

    return true;
  };

  /**
   * Set the given variable or property reference in the sandbox to the given
   * value.
   *
   * NB: NO MONKEYSHINES ALLOWED WHEN USING THIS METHOD.
   *
   * @param aNameSource
   *        A string naming the object that should be set.  It will be parsed
   *        as expected.  E.g., "foo.bar" will set the "bar" property of the
   *        sandbox-global "foo" object to the given value, "foo['baz']" will
   *        set its "baz" property, and so on.
   * @param a_x_Value
   *        The value it should be set to.
   *        NB: |a_x_Value| is untrusted.
   * @throws
   *        Error if you don't know what you're doing when you call this
   *        method.  Don't not know what you're doing when you call this
   *        method.
   *        NB: The thrown exception is untrusted. XXX Wrap it?
   */
  proto.setObject = function CE_SetObject(aNameSource, a_x_Value)
  {
    let splitPoints = CrummyTokenizer.findSplitPoints(aNameSource);
    if (splitPoints.length != 1) {
      throw new Error(
        "NO MONKEYSHINES WITH ConsoleEnvironment.prototype.setObject!" // XXX
      );
    }

    let sourceThings = CrummyTokenizer.splitIDs(aNameSource, splitPoints[0]);

    let lastPart = sourceThings.pop();

    // This could throw (see hacks/memberAccessTrust.js).  Let it.
    let baseObject = (sourceThings.length) ?
      this.resolveObject(sourceThings, true) :
      this.sandbox;

    // This could throw, too.  Let it.
    baseObject[lastPart.getValue()] = a_x_Value;
  };

  proto.resolveObject =
    function CE_ResolveObject(aSourceThings, aNoTrustIssues)
  {
    let baseObj = this.sandbox;
    let sourceThings = aSourceThings.slice(0);

    if (sourceThings.length && sourceThings[0].isEscaped()) {
      // sourceThings[0] isn't an identifier; it's a string literal--at least
      // it seems like it.  Let's check just to be safe.
      let parser;
      try {
        parser = sourceThings[0].getParser();
        if (parser.state != StringParser.ACCEPTING_STATE) {
          throw null;
        }
      }
      catch (ex) {
        let error =
          new Error("Expected " +
                    StringParser.uneval(sourceThings[0].getRawSource()) +
                    " to be a string, but caught exception during parsing: " +
                    ex);
        error.originalException = ex;
        throw error;
      }

      // baseObj needs to be the string itself.  Let's do it.
      let result = {};
      let evalSuccess =
        this.evalInput(sourceThings[0].getRawSource(), result, false);
      if (!evalSuccess) {
        throw result._x_exception;
      }

      baseObj = result._x_value;
      sourceThings.shift();
    }

    for (let i = 0, n = sourceThings.length; i < n && baseObj != null; ++i) {
      let id = sourceThings[i].getValue();
      let doTrust =
        !!aNoTrustIssues ||
        MemberAccessTrustHack.isMemberAccessTrustable(baseObj, id);
      if (!doTrust) {
        if (doTrust == undefined) {
          return undefined;
        }

        throw new Error("cannot trust " + StringParser.uneval(id) +
                        " property access"); // XXX
      }

      // This might throw.  Let it.
      // XXX [[GetOwnProperty]], [[Get]] magic?
      baseObj = baseObj[id];
    }

    return baseObj;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// nsIObserver

  /**
   * @param aSubject
   *        The console event.  Use |wrappedJSObject|.
   * @param aTopic
   *        "console-api-log-event"
   * @param aData
   *        The alternate ID of the context containing the console object.
   * @see hacks/ConsoleAPI.js
   */
  proto.observe = function CE_Observe(aSubject, aTopic, aData)
  {
    switch (aTopic) {
      case "console-api-log-event":
        this._onConsoleAPILogEvent(aSubject, aTopic, aData);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param aSubject
   *        NB: |aSubject.wrappedJSObject|'s elements are untrusted.
   * @param aTopic
   * @param aData
   */
  proto._onConsoleAPILogEvent =
    function CE_OnConsoleAPILogEvent(aSubject, aTopic, aData)
  {
    let notification = aSubject.wrappedJSObject;

    // Is it for us?
    if (this._x_context instanceof Ci.nsIDOMWindow) {
      let origin = this._getOuterWindowWithId(notification.ID);
      // XXX The Web Console surfaces framed documents' calls, too.  Pref?
      if (!origin || origin != this._x_context) {
        // Nope.
        return;
      }
    }
    
    // Nothing fancy for now, just print it.
    switch (notification.level) {
      case "log":
      case "info":
      case "warn":
      case "error":
      case "debug":
        // NB: |notification.arguments| elements are untrusted.
        for (let i = 0, n = notification.arguments.length; i < n; ++i) {
          let _x_arg = notification.arguments[i];
          this.consoleWriter.writeValue(_x_arg);
        }
      break;
      case "trace":
        this.consoleWriter.writeTrace(notification.stacktrace);
      break;
    }
  };

  /**
   * Avoid "nsIDOMWindowUtils.getOuterWindowWithId is deprecated warnings.
   *
   * @param aID
   *        The ID from the "console-api-log-event" notification.
   */
  proto._getOuterWindowWithId = function CE_GetOuterWindowWithId(aID)
  {
    if ("getOuterWindowWithId" in Services.wm) {
      return Services.wm.getOuterWindowWithId(aID);
    }

    let win = new XPCNativeWrapper(this._x_context);
    let winUtils = win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(
      Ci.nsIDOMWindowUtils
    );

    return winUtils.getOuterWindowWithId(notification.ID);
  };
}
