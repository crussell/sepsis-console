/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const viewerURI =
  "chrome://sepsis-console/content/integration/inspector/viewer/console.xul";

const config = {
  canInspectApplication: true,
  lowPriority: true
};

function filter(aObject)
{
  return true;
}

function getLocalizedName(aLocale)
{
  return "JavaScript Console";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.ConsoleViewer(aPanel);
}

// XXX Remove when minimum supported si version is 0.4 or higher.
// |initialize| got changed in sepsis-inspector 0.4pre to |initializeViewer|,
// for consistency.
let initialize = initializeViewer;
