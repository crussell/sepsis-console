/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-console/content/utils/jsValueToString.jsm");
Cu.import("chrome://sepsis-console/content/utils/prefManager.jsm");
Cu.import("chrome://sepsis-console/content/utils/stringParser.jsm");
Cu.import("chrome://sepsis-inspector/content/utils/notificationManager.jsm");
Cu.import("chrome://sepsis-inspector/content/utils/targetChanger.jsm");

//////////////////////////////////////////////////////////////////////////////

function ConsoleViewer(aPanel)
{
  this.mPanel = aPanel;

  // See comment for |this._changeTarget|.
  if (!("changeTarget" in aPanel)) {
    this.mNotificationManager = new NotificationManager(this);
    this.mTargetChanger = new TargetChanger(this);
  }

  this.mInput = document.getElementById("txConsoleInput");
  this.mScrollback = document.getElementById("bxScrollback");

  this.mInputResizer = new ConsoleInputResizer(this.mInput, this.mScrollback);

  this.mStringBundle = document.getElementById("consoleStringBundle");

  this.consoleWriter =
    new ViewerConsoleWriter(this.mScrollback, this.mStringBundle);

  // We can't give this a valid environment right now, because the one we use
  // depends on what we're inspecting, and we won't know that until our
  // subject setter executes.  So we give it a null environment for now.  It's
  // okay, because we'll update it later, as soon as we get our first subject
  // (which should be soon), and it shouldn't need respond to anything before
  // that happens.
  this.mDropResponder = new DropResponder(null, this.consoleWriter);
  this.mScrollbackDragWatcher =
    new ScrollbackDragWatcher(this.consoleWriter, this.mDropResponder);

  // Same goes for this.
  this.mAutoCompleteHack = new AutoCompleteHack(
    this.mInput,
    null,
    this.consoleWriter,
    PrefManager
  );

  this.mInput.addEventListener("command", this, false);
  this.mScrollback.contentDocument.addEventListener("click", this, false);
}

let (proto = ConsoleViewer.prototype) {
  proto.mAutoCompleteHack = null;
  proto.mConsoleEnvironment = null;
  proto.mInput = null;
  proto.mInputResizer = null;
  proto.mPanel = null;
  proto.mNotificationManager = null;
  proto.mScrollback = null;
  proto.mStringBundle = null;
  proto.mTargetChanger = null;

  proto._x_subject = null;

  ////////////////////////////////////////////////////////////////////////////
  //// "interface" sepsisIViewer

  proto.target = null;

  /**
   * For backwards compatibility.  XXX Remove this when we drop support for
   * sepsis-inspector versions < 0.4.
   */
  proto.__defineGetter__("subject", function CV_Subject_Get()
  {
    return this._x_subject;
  });

  /**
   * For backwards compatibility.  XXX Remove this when we drop support for
   * sepsis-inspector versions < 0.4.
   */
  proto.__defineSetter__("subject", function CV_Subject_Set(a_x_Val)
  {
    return this.inspectObject(a_x_Val);
  });

  proto.inspectObject = function CV_InspectObject(a_x_Val)
  {
    // NB: |this._x_subject| and |a_x_Val| are untrusted.
    this._x_subject = XPCNativeWrapper.unwrap(a_x_Val);

    this._useContextForExecution(Cu.getGlobalForObject(this._x_subject));

    // NB: Simply setting properties on a sandbox will fire the sandbox
    // prototype's setters if it has them, so... don't do that.
    Object.defineProperty(this.mConsoleEnvironment.sandbox, "$0",
                          { value: this._x_subject, enumerable: true,
                            configurable: true, writable: true });

    // XXX l10n
    this.consoleWriter.print(this._x_subject, " is available as $0");

    this._changeTarget(this._x_subject);

    return this._x_subject;
  };

  proto.inspectApplication = function CV_InspectApplication()
  {
    this._x_subject = null;

    this._useContextForExecution(null);

    let sandbox = this.mConsoleEnvironment.sandbox;
    if (Object.prototype.hasOwnProperty.call(sandbox, "$0")) {
      delete sandbox["$0"];
    }

    this._changeTarget(sandbox);
  };

  proto.destroy = function CV_Destroy()
  {
    if (this.mConsoleEnvironment) {
      this.mConsoleEnvironment.destroy();
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function CV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "click":
        this._onClick(aEvent);
      break;
      case "command":
        this._onCommand(aEvent);
      break;
      default:
        Cu.reportError("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param a_x_NewContext
   *        NB: |a_x_NewContext| is untrusted.
   */
  proto._useContextForExecution =
    function CV_UseContextForExecution(a_x_NewContext)
  {
    // XXX Should we give an opportunity to salvage the locals from the old
    // sandbox?

    // If we've already got a console environment, we can re-use it and just
    // set its context to the new one.
    if (!this.mConsoleEnvironment) {
      // Nope.  The viewer probably just finished initializing.  Go ahead and
      // set the rest up.
      this.mConsoleEnvironment =
        new ConsoleEnvironment(a_x_NewContext, this.consoleWriter);

      // We have to set this up here, since we couldn't do it during
      // initialization.  See the comment in the constructor about first
      // initializing with a null environment.
      this.mAutoCompleteHack.consoleEnvironment = this.mConsoleEnvironment;
      this.mDropResponder.consoleEnvironment = this.mConsoleEnvironment;
    }
    else {
      this.mConsoleEnvironment.context = a_x_NewContext;
    }

    // Make sure we don't say "... in the context of null" if |a_x_NewContext|
    // is null.  Say the sandbox, instead.  XXX This has always been a big
    // lie.  We're always executing in a sandbox, which may or may not have
    // the context as its prototype.  But we're never actually executing *in*
    // |a_x_NewContext| itself.
    // XXX l10n
    this.consoleWriter.print(
      "Now executing in the context of ",
      a_x_NewContext || this.mConsoleEnvironment.sandbox
    );
  };

  /**
   * Compatibility wrapper method for changing viewer's target object.
   * XXX Remove this when we drop support for sepsis-inspector versions < 0.4.
   *
   * Viewers no longer need to participate in the observe/notify system, and
   * |TargetChanger| is deprecated in favor of a new |changeTarget| method on
   * the panel.
   *
   * @param aTarget
   */
  proto._changeTarget = function CV_ChangeTarget(aTarget)
  {
    if (this.mTargetChanger) {
      this.mTargetChanger.changeTarget(aTarget);
    }
    else {
      this.mPanel.changeTarget(aTarget);
    }
  };

  proto._onClick = function CV_OnClick(aEvent)
  {
    if (!aEvent.target.classList.contains("clickable")) {
      return;
    }

    if (aEvent.target.hasAttribute("filename")) {
      let line = aEvent.target.getAttribute("linenumber");
      let file = aEvent.target.getAttribute("filename");
      gViewSourceUtils.viewSource(file, null, null, line);
    }
    else {
      // NB: |_x_value| is untrusted.
      let _x_value = this.consoleWriter.getDraggableValue(aEvent.target);
      this._changeTarget(_x_value);
    }

    aEvent.preventDefault();
  };

  proto._onCommand = function CV_OnCommand(aEvent)
  {
    let result = {};
    let clearInput =
      this.mConsoleEnvironment.evalInput(this.mInput.value, result);
    // NB: |result._x_value| is untrusted.
    if (result._x_value !== undefined) {
      this._changeTarget(result._x_value);
    }

    // Don't clear the input if there was a problem evaluating its contents.
    if (!clearInput) {
      aEvent.preventDefault();
    }
  };
}
