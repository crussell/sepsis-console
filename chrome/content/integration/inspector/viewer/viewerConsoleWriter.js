/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * The viewer's console writer extends |ConsoleWriter| to make inspectable
 * values clickable, so they can be inspected in child panels or new Inspector
 * windows.
 *
 * @see utils/consoleWriter.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-console/content/hacks/viewerRegistryHack.jsm");

//////////////////////////////////////////////////////////////////////////////

/**
 * @constructor
 * @see ConsoleWriter
 */
function ViewerConsoleWriter(aScrollback, aStringBundle)
{
  ConsoleWriter.call(this, aScrollback, aStringBundle);
}

let (proto = Object.create(ConsoleWriter.prototype)) {
  ViewerConsoleWriter.prototype = proto;
  proto.constructor = ViewerConsoleWriter;

  /**
   * Override the |ConsoleWriter| method of the same name to return an element
   * that the viewer can style (using the "clickable" class) and listen for
   * click events for when the value is inspectable.  As with draggables, the
   * value can be accessed through the element's |_x_value| property.
   *
   * @param a_x_Value
   *        NB: |a_x_Value| is untrusted.
   * @return
   *        For an inspectable value, an element with the "clickable" class.
   *        Otherwise, the same as |ConsoleWriter._thingToOutputNode|'s
   *        return value.
   */
  proto._thingToOutputNode = function VCW_ThingToOutputNode(a_x_Value)
  {
    let node =
      ConsoleWriter.prototype._thingToOutputNode.call(this, a_x_Value);

    // NB: We have to check |node._x_value| here instead of |a_x_Value|,
    // because in the case of |writeValue| calls with string arguments, they
    // morph into |ConsoleWriter.StringWrapper| instances, and |node._x_value|
    // and |node._x_value| won't be the same.  With those, the former will be
    // unwrapped string, and the latter will be the wrapper object.
    let _x_value = this.getDraggableValue(node);
    if (ViewerRegistry.isInspectable(_x_value)) {
      // assert(node instanceof Ci.nsIDOMElement);
      node.classList.add("clickable");
    }

    return node;
  };
}
