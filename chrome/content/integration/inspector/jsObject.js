/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Overlay script for the sepsis-inspector's JS Object viewer.
 */

//////////////////////////////////////////////////////////////////////////////

sepsis.dispatchCommand = function IO_DispatchCommand(aCommandName)
{
  var controller =
    document.commandDispatcher.getControllerForCommand(aCommandName);

  if (controller && controller.isCommandEnabled(aCommandName)) {
    controller.doCommand(aCommandName);
  }
};

/**
 * Update a command's disabled state.
 *
 * @param aCommandName
 *        The name of the command.  This should be the element ID of a
 *        XUL command element.
 */
sepsis.updateCommand = function IO_UpdateCommand(aCommandName)
{
  var command = document.getElementById(aCommandName);
  if (command) {
    let controller =
      document.commandDispatcher.getControllerForCommand(aCommandName);

    if (controller && controller.isCommandEnabled(aCommandName)) {
      command.removeAttribute("disabled");
    }
    else {
      command.setAttribute("disabled", "true");
    }
  }
};

sepsis.supportsCommand = function IO_SupportsCommand(aCommandName)
{
  switch (aCommandName) {
    case "cmd_evaluateJS":
      return true;
    break;
  }

  return false;
};

sepsis.isCommandEnabled = function IO_IsCommandEnabled(aCommandName)
{
  switch (aCommandName) {
    case "cmd_evaluateJS":
      return sepsis.getContext() != null;
    break;
    default:
      Components.utils.reportError("wat.  Command: " + aCommandName);
    break;
  }

  return false;
};

sepsis.doCommand = function IO_DoCommand(aCommandName)
{
  switch (aCommandName) {
    case "cmd_evaluateJS":
      this.targetSelectedObject();
    break;
    default:
      Components.utils.reportError("wat.  Command: " + aCommandName);
    break;
  }
};

sepsis.onEvent = function IO_OnEvent(aEventName)
{
};

sepsis.getContext = function IO_GetContext()
{
  var context;
  // NB: |getGlobalForObject| can't handle primitives.
  try {
    context = Components.utils.getGlobalForObject(viewer.target);
  }
  catch (ex) {
    context = null;
  }

  return context;
};

sepsis.targetSelectedObject = function IO_TargetSelectedObject()
{
  this.openConsoleForObject(viewer.target, this.getContext());
};

//////////////////////////////////////////////////////////////////////////////

sepsis.initializeInspectorOverlay = function IO_InitializeInspectorOverlay()
{
  document.commandDispatcher.getControllers().appendController(sepsis);
};

window.addEventListener("load", sepsis.initializeInspectorOverlay, false);
