/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * The only thing we do here is to cause the "Evaluate Javascript" item in the
 * viewer's context menu to open a console instead of DOM Inspector's inferior
 * dialog-based evaluator.
 *
 * Components.utils.getGlobalForObject can't work with primitives, so the item
 * is disabled for those values.  The DOM Inspector's own evaluator can handle
 * those items, so we are technically removing some functionality.  Outside of
 * using the feature when motivated by something similar to the purpose of
 * copy and paste (i.e., to reduce typing, and in a few narrow cases at that),
 * I can't think of a single use case for wanting to do something with a
 * primitive--they're immutable anyway.  So hopefully it's not so sorely
 * missed.  If it is we provide a pref to disable ourselves (albeit requiring
 * a viewer reload):
 *
 *   sepsis.console.integration.dominspector.overrideEval
 *
 * We augment the global |sepsis| object defined in hooks.js.  Do note that
 * the object is context-global, not app-global; we aren't munging the object
 * for everyone, just what the JavaScript Object viewer can see.
 */

sepsis.initializeDOMIOverlay = function DIO_InitializeDOMIOverlay()
{
  // Don't do anything if the prefs say not to.
  let (tmpScope = Object.create(null)) {
    Components.utils.import(
      "chrome://sepsis-console/content/utils/prefUtils.jsm",
      tmpScope
    );

    let prefs =
      new tmpScope.PrefUtils("sepsis.console.integration.dominspector.");
    if (!prefs.getPref("overrideEval")) {
      return;
    }
  }

  // Override the context menu to invoke our cmdSepsisConsole instead of
  // cmdEvalExpr.
  let (menu = document.getElementById("popupContext")) {
    let item = menu.firstChild;
    while (item) {
      if (item.getAttribute("command") == "cmdEvalExpr") {
        item.setAttribute("command", "cmdSepsisConsole");
        break;
      }
      item = item.nextSibling;
    }

    if (!item) {
      Components.utils.reportError("Unable to override cmdEvalExpr");
      return;
    }
  }

  // Override the viewer method that gets called for various events and is
  // responsible for determining commands' disabled state.
  let (isCommandEnabled = viewer.isCommandEnabled) {
    viewer.isCommandEnabled = function DIO_IsCommandEnabled(aCommand)
    {
      if (aCommand == "cmdSepsisConsole") {
        return sepsis.getContext() != null;
      }

      return isCommandEnabled.call(viewer, aCommand);
    };
  }

  // Before we are called, updateAllCommands will have already been called
  // once and disabled our cmdSepsisConsole command, since we weren't
  // initialized yet.  So force another update.
  viewer.updateAllCommands();
};

sepsis.getContext = function DIO_GetContext()
{
  var context;
  try {
    context = Components.utils.getGlobalForObject(viewer.getSelectedObject());
  }
  catch (ex) {
    context = null;
  }

  return context;
};

sepsis.targetSelectedObject = function DIO_TargetSelectedObject()
{
  this.openConsoleForObject(viewer.getSelectedObject(), this.getContext());
};

window.addEventListener("load", sepsis.initializeDOMIOverlay, false);
