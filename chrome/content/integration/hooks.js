/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

if (!("sepsis" in window)) {
  window.sepsis = {};
}

sepsis.openConsole = function Sepsis_OpenConsole(aContext)
{
  return window.openDialog("chrome://sepsis-console/content/main/console.xul",
                           "_blank", "chrome,all,dialog=no", aContext);
};

sepsis.openConsoleForObject =
  function Sepsis_OpenConsoleForObject(aObject, aContext)
{
  var observer = {
    consoleWin: null,

    observerService: 
      Components.classes["@mozilla.org/observer-service;1"].
        getService(Components.interfaces.nsIObserverService),

    observe: function DOI_Observe(aSubject, aTopic, aData)
    {
      // Exceptions raised here don't propogate out past the observer
      // service's calling frame. (XXX I think that's what happens here.  In
      // any case... --crussell)  If anything goes wrong, by default nothing
      // appears in the error console.  That's no good.  We need to ensure
      // that it will.
      try {
        if (aTopic != "sepsis-console-instantiated" ||
            aSubject != this.consoleWin) {
          return;
        }

        this.consoleWin.app.consoleEnvironment.sandbox.target = aObject;

        this.observerService.removeObserver(this,
                                            "sepsis-console-instantiated");
      }
      catch (ex) {
        Components.utils.reportError(ex);
      }
    }
  };

  observer.observerService.addObserver(observer,
                                       "sepsis-console-instantiated", false);

  let (context) {
    context = (aContext === undefined) ?
      Components.utils.getGlobalForObject(aObject) :
      aContext;

    return observer.consoleWin = this.openConsole(context);
  }
};
