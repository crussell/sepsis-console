/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Augment the |sepsis| object to provide a top-level command handler.  To
 * determine in what type of context the console should execute its input, we
 * read the pref:
 *
 *   sepsis.console.integration.<aAppName>.context
 *
 * Possible values are:
 *             0: Use an empty sandbox for the console's execution context.
 *                It will have chrome privileges.  XXX Why?  It seems that
 *                ConsoleEnvironment is buggy.  Check with bz.
 *   (default) 1: Use the current content, similar to DOM Inspector and the
 *                devtools Web Console.
 *             2: Use the application's own window as the execution context.
 */

sepsis.openConsoleByPref = function GO_OpenConsoleByPref(aAppName)
{
  if (aAppName == undefined) {
    throw new Error("app name must be provided");
  }

  if (!this.PrefUtils) {
    let tmpScope = Components.utils.import(
      "chrome://sepsis-console/content/utils/prefUtils.jsm",
      Object.create(null)
    );
    this.PrefUtils = tmpScope.PrefUtils;
  }

  var context;
  var prefs =
    new this.PrefUtils("sepsis.console.integration." + aAppName + ".");
  switch (prefs.getPref("context")) {
    // Empty sandbox
    case 0:
      // No op; context undefined.
    break;

    // Page context
    case 1:
      context = content;
    break;

    // App window context
    case 2:
      context = window;
    break;

    default:
      Components.utils.reportError(
        "unrecognized value for pref sepsis.integration." + aAppName +
        ".context: " + prefs.getPref("context")
      );
    break;
  }

  sepsis.openConsole(context);
};
