/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global variables

var EXPORTED_SYMBOLS = [ "PrefManager" ];

const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-console/content/utils/prefUtils.jsm");

//////////////////////////////////////////////////////////////////////////////
//// PrefManager

/**
 * A service with useful wrapper methods to make dealing with nsIPreferences
 * easier, as well as making sure prefs which should stay synchronized, uh...
 * do.
 *
 * The service is started at import and observers remain intact until
 * application shutdown.
 * XXX Remove observers when last console window is closed?
 */
var PrefManager = {
  CUSTOM_PROFILE: 1,
  BASH_PROFILE: 2,
  WEBCONSOLE_PROFILE: 3,
  VIM_PROFILE: 4,

  TAB: 1,
  CONSECUTIVE_TAB: 2,
  INPUT: 3,

  mPrefUtils: null,
  mIgnoreActivationChanges: null,

  init: function PM_Init()
  {
    this.mIgnoreActivationChanges = false;

    this.mPrefUtils = new PrefUtils("sepsis.console.");
    this.mPrefUtils.addObserver("", this);

    // Let us get ourselves to a sane state in case any of the prefs have been
    // twiddled while no one was watching.
    try {
      this._ensureProfilePrefs();
    }
    catch (ex) {
      Cu.reportError(ex);
    }
  },

  getPref: function PM_GetPref(aName)
  {
    return this.mPrefUtils.getPref(aName);
  },

  /**
   * Reads sepsis.console.autocomplete.activation.* prefs to determine which
   * responders to execute for the given activation type.
   *
   * @param aActivationType
   *        One of PrefManager.TAB, PrefManager.CONSECUTIVE_TAB, or
   *        PrefManager.INPUT.
   * @return
   *        An array of strings naming the responders for the given activation
   *        type.
   */
  getCompletionResponderNames:
    function PM_GetCompletionResponderNames(aActivationType)
  {
    var prefName = "autocomplete.activation.";
    switch (aActivationType) {
      case PrefManager.TAB:
        prefName += "tab";
      break;
      case PrefManager.CONSECUTIVE_TAB:
        prefName += "consecutivetab";
      break;
      case PrefManager.INPUT:
        prefName += "input";
      break;
      default:
        throw new Error("Unknown autocomplete activation type: " +
                        aActivationType);
      break;
    }

    var prefValue = this.mPrefUtils.getPref(prefName);
    var responderNames = prefValue && prefValue.split(",");

    return responderNames || [];
  },

  // nsIObserver
  observe: function PM_Observe(aSubject, aTopic, aData)
  {
    if (aData == "profile") {
      // The profile pref changed.  Set the other prefs accordingly.
      this._ensureProfilePrefs();
    }
    else if (aData.indexOf("activation.") == 0) {
      if (!this.mIgnoreActivationChanges) {
        this.mPrefUtils.setPref(
          "autocomplete.profile", PrefManager.CUSTOM_PROFILE
        );
      }
    }
  },

  /**
   * Called at initialization or after a autocomplete profile change to ensure
   * that the activation prefs match the presets for the current profile.
   *
   * E.g., if sepsis.console.autocomplete.profile is changed,
   * sepsis.console.autocomplete.activation.tab et cetera need to be updated
   * to name the responders for that profile type.
   */
  _ensureProfilePrefs: function PM_EnsureProfilePrefs()
  {
    // We're about to set the activation prefs to the built-in profile types'
    // presets.  This will end up triggering |observe|.  Let it know to ignore
    // these changes.
    this.mIgnoreActivationChanges = true;

    var profileType = this.mPrefUtils.getPref("autocomplete.profile");
    switch (profileType) {
      case PrefManager.CUSTOM_PROFILE:
        // No presets for this type; no op.
      break;
      case PrefManager.BASH_PROFILE:
        this.mPrefUtils.setPref(
          "autocomplete.activation.tab",
          "AmbiguousPartialCompletion,UnambiguousCompletion"
        );
        this.mPrefUtils.setPref(
          "autocomplete.activation.consecutivetab", "AmbiguousPrinting"
        );
        this.mPrefUtils.setPref(
          "autocomplete.activation.input", ""
        );
      break;
      case PrefManager.WEBCONSOLE_PROFILE:
        this.mPrefUtils.setPref(
          "autocomplete.activation.tab", "UnambiguousCompletion"
        );
        this.mPrefUtils.setPref(
          "autocomplete.activation.consecutivetab", ""
        );
        this.mPrefUtils.setPref(
          "autocomplete.activation.input", "AmbiguousPopup,UnambiguousShadow"
        );
      break;
      case PrefManager.VIM_PROFILE:
        this.mPrefUtils.setPref(
          "autocomplete.activation.tab",
          "AmbiguousFullCompletion,UnambiguousCompletion"
        );
        this.mPrefUtils.setPref(
          "autocomplete.activation.consecutivetab", ""
        );
        this.mPrefUtils.setPref(
          "autocomplete.activation.input", ""
        );
      break;
      default:
        this.mIgnoreActivationChanges = false;
        throw new Error("Unknown profile type: " + profileType);
      break;
    }

    this.mIgnoreActivationChanges = false;
  }
};

PrefManager.init();
