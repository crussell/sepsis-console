/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Shared class that implements |valueDroppedOnID|.  (The method is called by
 * the |ScrollbackDragWatcher| in response to "drop" events.)
 *
 * @see utils/scrollbackDragWatcher.js
 */

/**
 * @param aConsoleEnvironment
 *        |ConsoleEnvironment| instance.  This will be used to do name
 *        resolution of the ID when |valueDroppedOnID| is called.
 * @param aConsoleWriter
 *        |ConsoleWriter| instance.  Used to write feedback (such as drop
 *        success or failure) to the console.
 */
function DropResponder(aConsoleEnvironment, aConsoleWriter)
{
  this.consoleEnvironment = aConsoleEnvironment;
  this.mConsoleWriter = aConsoleWriter;
}

let (proto = DropResponder.prototype) {
  proto.mConsoleWriter = null;

  proto.consoleEnvironment = null;

  /**
   * @param a_x_Value
   *        NB: |a_x_Value| is untrusted.
   * @param aNameSource
   *        A string whose value is a named reference to the thing that should
   *        be set to the given value in the console environment's sandbox.
   * @see ConsoleEnvironment.prototype.setObject
   */
  proto.valueDroppedOnID =
    function DR_ValueDroppedOnID(a_x_Value, aNameSource)
  {
    try {
      this.consoleEnvironment.setObject(aNameSource, a_x_Value);
    }
    catch (_x_ex) {
      // NB: |_x_ex| is untrusted.
      // XXX
      this.mConsoleWriter.print("drop failed: ", _x_ex, "\n",
                                String(_x_ex.stack));
      return;
    }

    let _x_valueOrWrapper = (typeof(a_x_Value) == "string") ?
      new this.mConsoleWriter.StringWrapper(a_x_Value) :
      a_x_Value;

    // XXX l10n
    this.mConsoleWriter.print(
      _x_valueOrWrapper, " is available as " + aNameSource
    );
  };
}
