/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * 
 * A |ConsoleWriter| has high-level methods for responding to requests like:
 * - This input string was just evaled.  Please "echo" it to the scrollback.
 * - Write these evaled results to the console.
 * - Here's a stack trace.  Format it nicely and print it to the console.
 * - Print this other stuff to the console.
 *
 * The console has a scrollback with an |output| method that takes DOM nodes.
 * Various areas of the codebase want to be able to write to the console.
 * Instead of just handing them the scrollback and requiring them all to
 * generate their own nodes, we expose some high-level, purpose-built methods
 * here and hand out the |ConsoleWriter| instance for the scrollback.
 *
 * XXX Explain draggables and use of the writer methods, and how wrappers come
 * into play.
 *
 * This can be easily extended, too, as in the case of |ViewerConsoleWriter|.
 *
 * @see bindings/scrollback.xml
 * @see integration/inspector/viewer/viewerConsoleWriter.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-console/content/utils/jsValueToString.jsm");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aScrollback
 *        The scrollback element that we will be writing to.
 * @param aStringBundle
 *        The stringbundle element for the bundle containing strings for
 *        writing feedback messages to the console.
 * @see bindings/scrollback.xml
 */
function ConsoleWriter(aScrollback, aStringBundle)
{
  this.mScrollback = aScrollback;
  this.mStringBundle = aStringBundle;

  this.mValueStore = [];
}

/**
 * Used for wrapping string literals that get passed through |writeValue| so
 * they'll show up escaped when printed, but their draggables will still be
 * associated with the right value.
 *
 * For internal use.
 *
 * @param aString
 *        The string to wrap.  *Must* be a primitive string.
 * @throws
 *        ConsoleWriter.NotAStringError
 * @see ConsoleWriter.prototype.writeValue
 * @constructor
 */
ConsoleWriter.StringWrapper = function CW_StringWrapper(aString)
{
  if (typeof(aString) != "string") {
    throw new ConsoleWriter.NotAStringError();
  }

  this.mValue = aString;
};

let (swproto = ConsoleWriter.StringWrapper.prototype) {
  swproto.mValue = null;

  /**
   * @return
   *        A string describing an escaped form of the wrapped value.
   */
  swproto.toString = function CW_SW_ToString()
  {
    return jsValueToString(this.mValue);
  };

  /**
   * @return
   *        The wrapped (read: actual) value.
   */
  swproto.valueOf = function CW_SW_ValueOf()
  {
    return this.mValue;
  };
}

ConsoleWriter.NotAStringError = function CW_NotAStringError()
{
  return new TypeError(
    "Implicit string conversion not allowed here.  Argument must already " +
    "be a string."
  );
};

ConsoleWriter.ErrorWrapper = function CW_ErrorWrapper(aError)
{
  this.mError = aError;
};

let (ewproto = ConsoleWriter.ErrorWrapper.prototype) {
  ewproto.toString = function CW_EW_ToString()
  {
    return Object.prototype.toString.call(this.mError) + " : " +
           Error.prototype.toString.call(this.mError);
  };

  ewproto.valueOf = function CW_EW_ValueOf()
  {
    return this.mError;
  };
}

let (proto = ConsoleWriter.prototype) {
  proto.mScrollback = null;
  proto.mStringBundle = null;
  proto.mValueStore = null;

  proto.ErrorWrapper = ConsoleWriter.ErrorWrapper;
  proto.NotAStringError = ConsoleWriter.NotAStringError;
  proto.StringWrapper = ConsoleWriter.StringWrapper;

  proto.__defineGetter__("scrollback", function CW_Scrollback_Get()
  {
    return this.mScrollback;
  });

  /**
   * @param ...arguments
   *        Values that will be converted to strings and printed to console.
   *        Strings are output to the console raw (i.e., unquoted).  Non-
   *        strings may be further processed in ways specific to the value.
   *        NB: |arguments|'s elements are untrusted.
   */
  proto.print = function CW_Print()
  {
    let nodes = [];
    for (let i = 0, n = arguments.length; i < n; ++i) {
      // NB: |_x_arg| is untrusted.
      let _x_arg = arguments[i];
      if (typeof (_x_arg) == "string") {
        nodes.push(this._createTextNode(_x_arg));
      }
      else {
        nodes.push(this._thingToOutputNode(_x_arg));
      }
    }

    this.mScrollback.output.apply(this.mScrollback, nodes);
  };

  /**
   * Special-purpose method for writing the value of the console input to the
   * scrollback.
   *
   * When we echo a line like |let fum = 'omghax'| to the scrollback, we want
   * our drag responder accept the "fum" bit as a drop target.  Good?
   *
   * Now supposed the scrollback has echoed a line like |foo.bar = 'baz'|.  We
   * want the "foo" bit to be a drop target, and the ".bar" bit, too.  And the
   * way we want that to work is that when dragging to the "foo" bit, it gets
   * highlighted, and if dragging to the ".bar" bit, the fully-qualified
   * "foo.bar" gets highlighted.
   *
   * The way this works is that named references are wrapped in an .outer span
   * and an .inner span, where the .inner contains a text node with its
   * |textContent| naming the identifier.  For qualified names, the .inner
   * will be preceded by another .outer (itself containing its own .inner and
   * text node).  They're kind of like cons pairs, but backwards.
   * 
   * So for "foo.bar", it would look like:
   *
   *   <span class="thing outer">
   *     <span class="thing outer">
   *       <span class="thing inner">foo</span>
   *     </span>
   *     <span class="thing inner">.bar</span>
   *   </span>
   * 
   * Likewise, for "foo.bar.baz", we'd have that node sequence, followed by
   * another .inner containing the ".baz" text, all wrapped in another .outer.
   *
   * To apply an effect to the entire qualified name (e.g., "foo.bar"), given
   * the inner for the ending part (e.g., ".bar"), just style its outer.
   *
   * For drag and drop, we work in conjunction with the app's 
   * |ScrollbackDragWatcher| instance, which will take care of setting the
   * "dragover" attribute on the appropriate .outer.
   *
   * @param aInputString
   *        The evaled input to be echoed to the scrollback.
   * @see utils/scrollbackDragWatcher.js
   * @see contentaccessible/skin/classic/scrollback.css
   */
  proto.writeInput = function CW_WriteInput(aInputString)
  {
    let splot = this._splitInput(aInputString);
    let nodes = [];
    let lastOuter = null;
    for (let i = 0, n = splot.length; i < n; ++i) {
      if (!splot[i]) {
        // Empty string.
        continue;
      }

      let textNode = this._createTextNode(splot[i]);

      // In |splot|, we've got an array of strings, where every other string
      // is a non-identifier, then an identifier or a string literal (starting
      // with a non-identifier as the first element.)
      let doWrap = 
        (i % 2) &&                                // Skip the non-identifiers
        !CrummyTokenizer.isQuoted(splot[i][0]) && // ... and string literals
        !idUtils.isReservedWord(splot[i]);        // ... and reserved words
      // XXX When the reserved word appears as part of a qualified property
      // reference and not a bare word, it should be fine.  We should also let
      // our autocomplete implementation in on this secret.

      if (doWrap) {
        let inner = this.mScrollback.contentDocument.createElement("span");
        inner.classList.add("thing", "inner");
        let outer = this.mScrollback.contentDocument.createElement("span");
        outer.classList.add("thing", "outer");

        if (lastOuter) {
          outer.appendChild(lastOuter);
        }
        inner.appendChild(textNode);
        outer.appendChild(inner);

        lastOuter = outer;
      }
      else {
        if (lastOuter) {
          nodes.push(lastOuter);
          lastOuter = null;
        }
        nodes.push(textNode);
      }
    }

    // Make sure we don't forget this, since inside the loop we don't push the
    // outers until we find the next iteration where we get |!doWrap|.  If we
    // don't do this, and the input ended with a wrappable identifier, the
    // output won't include its outer.
    if (lastOuter) {
      nodes.push(lastOuter);
    }

    this.mScrollback.output.apply(this.mScrollback, nodes);
  };

  /**
   * @param ...arguments
   *        Values that will be converted to strings and printed to console.
   *        String values will be escaped.
   *        NB: |arguments|'s elements are untrusted.
   */
  proto.writeValue = function CW_WriteValue()
  {
    let processed = [];
    for (let i = 0, n = arguments.length; i < n; ++i) {
      // NB: |_x_arg| is untrusted.
      let _x_arg = arguments[i];
      if (typeof(_x_arg) == "string") {
        processed.push(new ConsoleWriter.StringWrapper(_x_arg));
      }
      else {
        let classString = Object.prototype.toString.call(_x_arg);
        if (classString == "[object Error]" ||
            classString == "[object Exception]") {
          processed.push(new ConsoleWriter.ErrorWrapper(_x_arg));
        }
        else {
          processed.push(_x_arg);
        }
      }
    }

    this.print.apply(this, processed);
  };

  /**
   * Write the frames from the given stack trace to the console.
   *
   * @param aStackTrace
   *        Stack trace object matching the format of the one created by the
   *        console API component.
   * @see /components/ConsoleAPI.js
   */
  proto.writeTrace = function CW_WriteTrace(aStackTrace)
  {
    let nodes = [ this._createTextNode(
      this.mStringBundle.getString("consoleDotTracePre.label")
    ) ];
    for (let i = 0, n = aStackTrace.length; i < n; ++i) {
      nodes = nodes.concat(this._frameInfoToNodes(aStackTrace[i]));
    }

    this.mScrollback.output.apply(this.mScrollback, nodes);
  };

  /**
   * Nodes in the scrollback document with the "draggable" attribute--at
   * least, the ones we created--have an associated value for drag-and-drop. 
   * Get at it.
   *
   * @param aNode
   *        A draggable DOM node.  Must have been created with
   *        |_createDraggableNode| or one of the high-level methods that end
   *        up calling it, like |writeValue|.
   * @return
   *        The JS value associated with the node; generally the value it was
   *        created with.  (Generally.  String wrappers are special-cased.)
   *        NB: The return value is untrusted.
   * @see ConsoleWriter.prototype.writeValue
   * @see ConsoleWriter.StringWrapper
   */
  proto.getDraggableValue = function CW_GetDraggableValue(aNode)
  {
    let vid = aNode.getAttribute("vid");
    if (!vid) {
      throw new Error("Node has no value ID.");
    }
    return this.mValueStore[vid];
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Called by |print| to process non-string arguments.
   *
   * @param a_x_Value
   *        NB: |a_x_Value| is untrusted.
   * @return
   *        A node whose |textContent| is the given value in string form.
   */
  proto._thingToOutputNode = function CW_ThingToOutputNode(a_x_Value)
  {
    let node = this._createDraggableNode(a_x_Value);

    // For string and error wrappers, the draggable's value needs to get fixed
    // up to be the thing it's wrapping instead of the wrapper itself.
    if (a_x_Value instanceof ConsoleWriter.StringWrapper ||
        a_x_Value instanceof ConsoleWriter.ErrorWrapper) {
      this.mValueStore[node.getAttribute("vid")] = a_x_Value.valueOf();
    }

    return node;
  };

  /**
   * @param aString
   *        A string to be used as the node's |textContent|.  *Must* be a
   *        primitive string.
   * @throws
   *        ConsoleWriter.NotAStringError
   */
  proto._createTextNode = function CW_CreateTextNode(aString)
  {
    if (typeof(aString) != "string") {
      throw new this.NotAStringError();
    }

    return this.mScrollback.contentDocument.createTextNode(aString);
  };

  /*
   * XXX Instead of fixing up the value store for specially wrapped values in
   * |_thingToOutputNode|, this method should take an optional |aTextContent|
   * argument.
   *
   * @param a_x_Value
   *        NB: |a_x_Value| is untrusted.
   * @return
   *        A node with a "draggable" attribute, and a "vid" attribute whose
   *        value can be used to look up the associated object |a_x_Value|
   *        using |getDraggableValue|.
   */
  proto._createDraggableNode = function CW_CreateDraggableNode(a_x_Value)
  {
    let textContent = this._getPrintStringForThing(a_x_Value);
    let node = this.mScrollback.contentDocument.createElement("a");
    node.setAttribute("draggable", "true");
    node.setAttribute("vid", this.mValueStore.length);
    this.mValueStore.push(a_x_Value);
    node.appendChild(this._createTextNode(textContent));
    return node;
  };

  /**
   * TODO
   * - Use the constructor name, if we can, for objects from script, instead
   *   of the unhelpful "[object Object]" string.
   * - It would be nice to break down the non-native-function case into
   *   recognizing more specific ones, like stubbed-function ("{ }"), and
   *   returns-a-constant (e.g., "{ return 0; }").
   *
   * @param a_x_Value
   *        NB: |a_x_Value| is untrusted.
   */
  proto._getPrintStringForThing =
    function CW_GetPrintStringForThing(a_x_Value)
  {
    if (typeof(a_x_Value) == "function") {
      // NB: |a_x_Value| IS STILL UNTRUSTABLE, even for expected-unwritable
      // values like |name|, because proxies.
      //
      // ... Also, it may be just some object that has a [[Call]], like an
      // embed or object element.
      let stringed;
      try {
        // XXX If |a_x_Value| is a (function) proxy, |stringed| will end up as
        // the source of the target function here, which is maybe not what we
        // want.  (I think what we want is, even if we can't get at the source
        // of the proxy handler's |apply|, to at least indicate that whatever
        // |Function.prototype.toString| reports is not necessarily true.
        // --crussell)
        stringed = Function.prototype.toString.call(a_x_Value);
      }
      catch (ex) {
        // Swallow it.  We want to fall through past the following
        // conditional, to the common object-handling code path below.
      }

      if (stringed !== undefined) {
        return this._getPrintStringForFunction(a_x_Value, stringed);
      }
    }

    // We allow |"function"| here, because |Function.prototype.toString| won't
    // necessarily work above for all objects with a [[Call]].
    if ((typeof(a_x_Value) == "object" || typeof(a_x_Value) == "function") &&
        a_x_Value != null &&
        !(a_x_Value instanceof ConsoleWriter.StringWrapper) &&
        !(a_x_Value instanceof ConsoleWriter.ErrorWrapper)) {
      return Object.prototype.toString.call(a_x_Value);
    }

    return String(a_x_Value);
  };

  /**
   * @param a_x_Function
   *        NB: |a_x_Function| is untrusted.
   * @return
   *        A string representation of the given function.
   * @throws
   *        Error in certain cases where |a_x_Function| is not a real
   *        function (e.g., an embed element, which has a [[Call]], and so
   *        report |typeof| reports as "function").
   */
  proto._getPrintStringForFunction =
    function PCW_GetPrintStringForFunction(a_x_Function)
  {
    let source = Function.prototype.toString.call(a_x_Function);
    let lines = source.split("\n");
    if (lines.length == 3 && lines[1] == "    [native code]" &&
        lines[2] == "}") {
      return source.substr(0, source.indexOf("{")) + "{ [native code] }";
    }
    // NB: Don't use |substr| to |.indexOf("{")| here, like we do above,
    // because that doesn't handle short functions or arrow functions, e.g.:
    //
    //   function () foo
    //   x => foo
    //
    // We can't rely on |source.indexOf(")") + 1|, either, because it can
    // break with default parameters.  It would be nice if we didn't lose the
    // arg names because of this, but I don't feel like writing a full-on
    // FunctionExpression parser right now, especially without a stable spec
    // to work from and a near-compliant host engine to cross-reference.
    // --crussell
    let result = "function ";

    // NB: |_x_name| is untrusted.  See above.
    let (_x_name) {
      try {
        _x_name = a_x_Value.name;
      }
      catch (_x_ex) {
        // NB: |_x_ex| is untrusted.
        //
        // If |a_x_Value| is a proxy, we can get a "proxy must report the same
        // value for a non-writable, non-configurable property" error here, if
        // the author didn't implement proxies correctly.

        // XXX We really need a general-purpose routine to call for things
        // like "we got an exception because stuff is weird, but we can
        // recover from it anyway".  We just swallow it for now.
      }

      if (typeof(_x_name) == "string" &&
          idUtils.isIdentifierName(_x_name)) {
        result += _x_name;
      }
    }

    result += "() { [...] }";
    return result;
  };

  /**
   * @param aInputString
   *        The evaled string.
   * @return
   *        An array of alternating non-identifiers and identifiers/string
   *        literals.  (Starting with non-identifiers.)
   */
  proto._splitInput = function PCW_SplitInput(aInputString)
  {
    let splitPointSets = CrummyTokenizer.findSplitPoints(aInputString);

    // Start off with a beginning-of-string split point.  (See comment for
    // |CrummyTokenizer.splitAt|.)
    let spanIndexes = [ 0 ];
    for (let i = 0, n = splitPointSets.length; i < n; ++i) {
      let set = splitPointSets[i];
      for (let i = 0, n = set.length; i < n; ++i) {
        spanIndexes.push(set[i]);

        // In |writeInput|, we expect an alternating sequence of non-
        // identifiers and identifiers/string literals.  (See |i % 2|.)  But
        // if we use the points for |set| straight as they come, we won't get
        // that.
        //
        // Consider the case of
        //
        //   fum.omg.hax = 'nom';
        //
        // Our first set will be for the "fum.omg.hax" bit.  |CrummyTokenizer.
        // findSplitPoints| will give us the set:
        //
        //   fum.omg.hax   => [ 0, 3, 7, 11 ]
        //   ^  ^   ^   ^
        //
        // If we ignore the extra beginning-of-string split point we seeded
        // |spanIndexes| with for the time being and pass this array to
        // |CrummyTokenizer.splitAt|, it won't give us the alternating
        // sequence of non-identifiers and identifiers that we want; we'll get
        // back three strings, all of them identifiers.
        //
        // With the beginning-of-string split point, though, we'd be passing
        // [ 0, 0, 3, 7, 11 ] to |CrummyTokenizer.splitAt|, and we'd get back
        // four strings: one empty string, plus the three others.  In that
        // case, just as we want it to be, the first string will be the non-
        // identifier text that precedes the first identifier.  (It turns out
        // that string is empty, but it's true nonetheless).  Following that--
        // and just as we want--the next string it gives us is the name of
        // that identifier ("fum").
        //
        // The next in sequence, though, is the text of a (qualified)
        // identifier (".omg"), when what we want is some text describing a
        // non-identifer.  But there is no non-identifier text that follows
        // the "fum" bit and precedes the ".omg" bit.  Recall, however, that
        // empty string before "fum".  That gives us a hint about how we can
        // approach this.
        //
        // (I hope it's obvious what we're going to do.)
        //
        // So, notice that (again, with the beginning-of-string split point
        // this time) if we can pass in indexes for some zero-length spans,
        // giving it [ 0, 0, 3, 3, 7, 7, 11 ], we get back:
        //
        //   CrummyTokenizer.splitAt("fum.omg.hax", [ 0, 0, 3, 3, 7, 7, 11 ])
        //     => [ "", "fum", "", ".omg", "", ".hax" ]
        //
        // ... which turns out to be alternating sequence of non-identifiers
        // and identfiers, just like we want.
        //
        // That's easy to get to.  What we need to do is double up on the
        // intermediate split points (that is, the points that are neither
        // first nor last) in the set.  (Recall again that there are two 0
        // points present above because the first is from the beginning-of-
        // string-split point.)  This works for unqualified and well-qualified
        // names: for |foo|, for example, there are only two split points, so
        // there are no intermediates; for |this.really["long"].qualified.
        // property.reference|, it works just as well as it does for our
        // example case above.
        if (i > 0 && i < n - 1) {
          spanIndexes.push(set[i]);
        }
      }
    }
    // ... and end-of-string.
    spanIndexes.push(aInputString.length);

    // So, for completeness, for the full string from our example above, we'd
    // pass to |CrummyTokenizer.splitAt|:
    //   [ 0, 0, 3, 3, 7, 7, 11, 14, 19, 20 ]
    //
    // And we'd get back:
    //   [ "", "fum", "", "omg", "", "hax", " = ", "'nom'", ";" ].
    //
    // Notice that |'nom'| is not an identifier, but we did mention at the top
    // that we're expecting to handle an alternating sequence of non-
    // identifiers and identifiers *or* string literals.  For our purposes,
    // we just end up skipping the strings for the identifier-wrapping in
    // |writeInput|, but things are the way they are because |CrummyTokenizer.
    // findSplitPoints| is also used for autocomplete, which does want to know
    // about those strings.
    return CrummyTokenizer.splitAt(aInputString, spanIndexes);
  };

  /**
   * Create an element of the given name, for use in the scrollback document.
   *
   * @param aName
   *        The name of the element to be created.
   * @return
   *        The created element.
   */
  proto._createElement = function CW_CreateElement(aName)
  {
    return this.mScrollback.contentDocument.createElement(aName);
  };

  /**
   * Given a stack frame, get an array of nodes for outputting to the
   * scrollback document.
   *
   * @param aFrameInfo
   *        An object matching the interface of the stack frames created by
   *        the console API component.
   * @return
   *        An array of nsIDOMNodes representing the frame.
   */
  proto._frameInfoToNodes = function CW_FrameInfoToNodes(aFrameInfo)
  {
    let nodes = [ this._createElement("br"), this._createTextNode("  ") ];

    let functionNode = this._nodeFromStackFunction(aFrameInfo);
    if (functionNode) {
      nodes.push(functionNode);
    }
    nodes.push(this._nodeFromStackLocation(aFrameInfo));

    return nodes;
  };

  /**
   * @param aFrameInfo
   *        An object matching the interface of the stack frames created by
   *        the console API component.
   * @return
   *        A node representing the function name of the given frame, or null
   *        if none.
   */
  proto._nodeFromStackFunction = function CW_NodeFromStackFunction(aFrameInfo)
  {
    return (aFrameInfo.functionName) ?
      this._createTextNode(aFrameInfo.functionName + " @ ") :
      null;
  };

  /**
   * @param aFrameInfo
   *        An object matching the interface of the stack frames created by
   *        the console API component.
   * @return
   *        A node representing the URI and line number of the given frame.
   */
  proto._nodeFromStackLocation = function CW_nodeFromStackLocation(aFrameInfo)
  {
    let node = this.mScrollback.contentDocument.createElement("a");
    node.classList.add("clickable", "uri");
    node.appendChild(this._createTextNode(
      aFrameInfo.filename + ":" + aFrameInfo.lineNumber
    ));
    node.setAttribute("filename", aFrameInfo.filename);
    node.setAttribute("linenumber", aFrameInfo.lineNumber);
    return node;
  };
}
