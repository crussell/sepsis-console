/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * TODO:
 *   Figure out how to check Unicode character categories so we can support
 *   non-ASCII ids, and do it.
 */

var idUtils = {

  RESERVED_WORDS: [
    "break", "case", "catch", "continue", "debugger", "default", "delete",
    "do", "else", "finally", "for", "function", "if", "in", "instanceof",
    "new", "return", "switch", "this", "throw", "try", "typeof", "var",
    "void", "while", "with",
    // Future reserved words
    "class", "const", "enum", "export", "extends", "import", "super",
    // Strict mode future reserved words
    "implements", "interface", "let", "package", "private", "protected",
    "public", "static", "yield",
    // NullLiteral
    "null",
    // BooleanLiteral
    "true", "false"
  ],

  isReservedWord: function IU_IsReservedWord(aString)
  {
    return this.RESERVED_WORDS.indexOf(aString) >= 0;
  },

  /**
   * @param aID
   * @return
   *        True iff a property with the given name must be accessed with
   *        bracket notation.
   */
  idRequiresBracketedAccess: function IU_IDRequiresBracketedAccess(aID)
  {
    if (aID == "" || idUtils.isIdentifierName(aID)) {
      return this.isReservedWord(aID);
    }

    return false;
  },

  /**
   * NB: If the context allows for the identifier to be optional, caller
   * should explicitly check disjunction with the return value here.  E.g.,
   *
   *   idUtils.isIdentifierName(x)         => false
   *   !x || idUtils.isIdentifierName(x)   => true
   *
   * ... where |x| is the empty string.
   *
   * XXX ASCII-only right now.  Returns false if a non-ASCII character is
   * encountered. --crussell
   *
   * @param aName
   *        A string naming the potential identifier name.  NB: This parameter
   *        is not type-checked and will happily treat non-strings like
   *        strings.
   * return
   *       True iff the given name matches the IdentifierName production.
   *       NB: This includes reserved words.  See ES 5.1, §7.6.
   */
  isIdentifierName: function IU_IsIdentifierName(aName)
  {
    // NB: This will also return false for |aName == ""|, which is what we
    // want.
    if (!idUtils.isIdentifierStart(aName.charAt(0))) {
      return false;
    }

    // NB: |i = 1|
    for (let i = 1, n = aName.length; i < n; ++i) {
      if (!idUtils.isIdentifierPart(aName[i])) {
        return false;
      }
    }

    return true;
  },

  isIdentifierStart: function IU_IsIdentifierStart(aCharacter)
  {
    if (aCharacter == "$" || aCharacter == "_" ||
        idUtils.isUnicodeLetter(aCharacter)) {
      return true;
    }

    return false;
  },

  /**
   * XXX ASCII-only right now.  Returns false if a non-ASCII character is
   * given. --crussell
   */
  isUnicodeLetter: function IU_IsUnicodeLetter(aCharacter)
  {
    var charCode = aCharacter.charCodeAt(0);
    if ((charCode >= 65 && charCode <= 90) ||
        (charCode >= 97 && charCode <= 122)) {
      return true;
    }
    // XXX non-ASCII

    return false;
  },

  /**
   * XXX ASCII-only right now.  Returns false if a non-ASCII character is
   * given. --crussell
   */
  isIdentifierPart: function IU_IsIdentifierPart(aCharacter)
  {
    var charCode = aCharacter.charCodeAt(0);
    // XXX non-ASCII
    return idUtils.isIdentifierStart(aCharacter) ||
           (charCode >= 48 && charCode <= 57);
  }
};

//////////////////////////////////////////////////////////////////////////////
//// SourceThing

/**
 * Convenience data structure for wrapping source tokens corresponding to
 * identifiers or escaped strings that name identifiers.  The internal format
 * of the wrapped value (that is, escaped or unescaped) is required to be a
 * known known or a known unknown to consumers.  This class exists primarily
 * to enforce that and prevent slip-ups.  Several methods are provided to
 * cater to these two uses.
 *
 * @param aID
 *        A string containing the identifier to wrap.  This may be the name of
 *        the ID itself or an escaped string that, after being parsed, will
 *        name the ID.
 * @param aIsEscaped
 *        True if the wrapped value is given in escaped form.
 * @constructor
 */
idUtils.SourceThing = function IU_SourceThing(aID, aIsEscaped)
{
  this.mID = aID;
  this.mIsEscaped = !!aIsEscaped;
};

let (proto = idUtils.SourceThing.prototype) {
  proto.mID = null;

  /**
   * This method can be used by consumers to determine the internal format of
   * the wrapped value when it's not otherwise known.
   *
   * @return
   *        True if the internal format of the wrapped value appears to be an
   *        escaped string.
   */
  proto.isEscaped = function IU_ST_IsEscaped()
  {
    return this.mIsEscaped;
  };

  /**
   * Convenience method to parse the wrapped value and obtain a reference to
   * the StringParser used.
   *
   * Don't call this if |isEscaped| returns false, unless the value is known
   * to be an empty string.
   *
   * @throws
   *        The StringParser will throw some type of error if it encounters a
   *        problem parsing the wrapped value.
   * @see isEscaped
   */
  proto.getParser = function IU_ST_GetParser()
  {
    return new StringParser(this.mID);
  };

  /**
   * When the internal format of the wrapped value is known to be an escaped
   * string, this method can be used to get the source of that literal.
   *
   * Don't call this if |isEscaped| returns false, unless the value is known
   * to be an empty string.
   *
   * @return
   *        The source of the wrapped value, as typed.
   * @throws
   *        Error if the internal format of the wrapped value is not an
   *        escaped string.
   * @see isEscaped
   */
  proto.getRawSource = function IU_ST_GetRawSource()
  {
    if (this.mID && !this.isEscaped()) {
      throw new Error("ID not quoted");
    }

    return this.mID;
  };

  /**
   * When the internal format of the wrapped value is either unknown or known
   * but of no interest, this method can be used to get the ID, after
   * having been parsed if it needs to be.
   *
   * @param aGetKnown [optional]
   *        If true, the ID string's known value will be returned if the
   *        internal format of the wrapped value is an escaped sring.
   * @see StringParser.prototype.getKnownValue
   */
  proto.getValue = function IU_ST_GetValue(aGetKnown)
  {
    if (this.isEscaped()) {
      let parser = this.getParser();
      return (aGetKnown) ?
        parser.getKnownValue() :
        parser.value;
    }

    return this.mID;
  };
}
