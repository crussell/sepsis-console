/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

let CrummyTokenizer = {

  UNQUOTED_STATE: 1,
  QUOTED_STATE: 2,
  // TODO REGEXP_STATE? Maybe too hard.

  /**
   * Given the way this method works and how JS doesn't have a separate
   * character type, only strings, you can also pretend that this method is
   * really |isQuote|, if you want.
   *
   * @param aString
   * @return
   *        True iff the string's first character is a quote.
   */
  isQuoted: function CT_IsQuoted(aString)
  {
    let firstCharacter = aString.charAt(0);
    return firstCharacter == "'" || firstCharacter == "\"";
  },

  /**
   * The input string might contain multiple statements, complex expressions,
   * et cetera.  So we break it down to isolate the substring relevant to
   * completion.  For example, the search string could be something like:
   *
   *   o.__defineSetter__("value",
   *     function(aVal)
   *   {
   *     console.log("value set to " + aVal);
   *     console.log("q's position is " + q.cur
   *
   * (NB: We do support newlines in the argument like this.)
   *
   * We'd want to break this down in order to complete on "q.cur" (for which
   * we can present, for example, a "currentPosition" property name as a
   * match, if it exists).
   *
   * If we determine that the substring we're autocompleting on is for an
   * access of some property name beginning with "baz" on the |foo.bar| (or
   * |foo["bar"]|) object, the corresponding return values are:
   *
   *   case 1: foo.bar.baz      => [ x, x + 3, x + 3 + 4 ]
   *           ^  ^   ^
   *   case 2: foo["bar"].baz   => [ x, x + 3, x + 3 + 7 ]
   *           ^  ^      ^
   * ... where x refers to the position of the "f" in the |foo| part of the
   * given input string.
   *
   * NB: For a bracketed property access, neither the close quote nor the
   * close bracket constitute a split point.  So just as the following results
   * in two split points:
   *
   *   case 3: foo["bar     => [ x, x + 3 ]
   *           ^  ^
   * ... so, too, do the following:
   *
   *   case 4: foo["bar"    => [ x, x + 3 ]
   *           ^  ^
   *   case 5: foo["bar"]   => [ x, x + 3 ]
   *           ^  ^
   *
   * A split point only appears following a bracketed reference under the same
   * conditions as it would for an unbracketed reference: when a dot or
   * another open bracket appears (cf. case 2).
   */
  findSplitPoints: function CT_FindSplitPoint(aString)
  {
    let quote, parser;
    let result = [];
    let state = CrummyTokenizer.UNQUOTED_STATE;
    let substringStart = 0;
    let splitPoints = [ 0 ];

    for (let i = 0, n = aString.length; i < n; ++i) {
      let c = aString[i];
      if (state == CrummyTokenizer.UNQUOTED_STATE) {
        switch (c) {
          case "'":
          case "\"":
            parser = new StringParser(c);
            state = CrummyTokenizer.QUOTED_STATE;
          break;
          case ".":
            splitPoints.push(i);
          break;
          case "[":
            // We want to be able to support both dot and bracket notation;
            // these should get equal treatment from the user perspective:
            //   obj.prop
            //   obj["prop"]
            if (substringStart != i) {
              // Look ahead if we can.
              let lookAhead = aString.charAt(i + 1);
              if (!lookAhead || this.isQuoted(lookAhead)) {
                // We'll assume its simple bracket notation using a string
                // literal, until we find out otherwise (if ever).
                splitPoints.push(i);
                break;
              }
              // Else, fall through.
            }
            // If substringStart is the same as i, we assume (see the comment
            // below about unsupported code styles) that this is an array
            // literal and not a property access.  Fall through.
          default:
            // We can handle the following case:
            //   obj["prop"]
            //
            // But not:
            //   obj["prefix" + val]
            //
            // And the following styles are all legal and it would be nice to
            // support them, but right now we don't:
            //
            //   obj . prop
            //
            //   obj[ "prop" ]
            //
            //   obj.prop.otherprop
            //      .another
            //
            if (splitPoints.length > 1 &&
                aString[splitPoints[splitPoints.length - 1]] == "[") {
              if (c == "]") {
                continue;
              }
              // Else, we're going to reset.  It is possible that it's not a
              // complex property access (see the comment above about
              // unsupported code styles), but for now we handle this by just
              // abandoning hope.
            }

            let needsStart = substringStart == i;
            if ((needsStart && idUtils.isIdentifierStart(c)) ||
                (!needsStart && idUtils.isIdentifierPart(c))) {
              continue;
            }

            // Reset.

            if (substringStart != i) {
              splitPoints.push(i);
              result.push(splitPoints);
            }

            substringStart = i + 1;
            splitPoints = [ substringStart ];
          break;
        }
      }
      else { // -> state == CrummyTokenizer.QUOTED_STATE
        parser.feed(c);

        if (parser.state == StringParser.ACCEPTING_STATE) {
          state = CrummyTokenizer.UNQUOTED_STATE;
          parser = null;
        }
      }
    }

    // If |substringStart == aString.length|, then we managed to get to the
    // end of the input string without finding any suitable split points for
    // the last set.  In the final iteration of our loop, though, we seeded
    // |splitPoints| with |substringStart| as the first element, anticipating
    // that we'd find something suitable.  In that case, make sure we don't
    // include in the results that single-element array containing an invalid
    // index.
    //
    // 
    if (substringStart != aString.length) {
      result.push(splitPoints);
    }
    else {
      result.push([]);
    }

    return result;
  },

  /**
   * Extract the IDs from the given string using the given split points, and
   * return them in an array.
   *
   * Method consumers should pay special attention to two cases.
   *
   * If the first "identifier" is quoted, it's not actually an identifier at
   * all; it's a string literal.  E.g.,
   *
   *   "a".charCodeA[...]
   *
   * Additionally, if the last ID is being accessed through bracket notation,
   * the string literal that the last element corresponds to may be
   * unterminated.  It should be possible to safely parse all other fully
   * quoted references. XXX
   *
   * @param aString
   *        The search string the IDs should be extracted from.
   * @param aSplitPoints
   *        An array of split points.  This should be the array returned by
   *        |findSplitPoints| when called with the given string.
   * @return
   *        An array of SourceThings naming the IDs in the given string,
   *        carved up at the given split points.
   */
  splitIDs: function CT_SplitIDs(aString, aSplitPoints)
  {
    let splitPoints = aSplitPoints.slice(0);
    let ids = [];
    if (!splitPoints.length) {
      return ids;
    }

    let idStart = splitPoints.shift();
    // Add a fake split point representing the end of the string, so below we
    // don't have to special-case the last iteration of our loop too much.
    splitPoints.push(aString.length);

    for (let i = 0, n = splitPoints.length; i < n; ++i) {
      let idString = aString.substring(idStart, splitPoints[i]);
      let isQuoted = this.isQuoted(idString);
      let idThing = new idUtils.SourceThing(idString, isQuoted);

      // NB: If the current member is being accessed through bracket notation,
      // we need to make an adjustment so as not to include the close bracket,
      // if it's there.  (If |i| is 0, it's actually a string literal.  See
      // the method description above.)
      if (isQuoted && i > 0 && idString.slice(-1) == "]") {
        idThing = new idUtils.SourceThing(idString.slice(0, -1), true);
      }

      ids.push(idThing);

      // Prep idStart for the next iteration. Remember to skip the characters
      // that delimit identifier names (e.g., dot).
      idStart = splitPoints[i] + 1;
    }

    return ids;
  },

  /**
   * NB: This will *not* automatically give you the text between beginning-of-
   * string and the first index (the "prefix"), nor the text between the last
   * index and end-of-string (the "suffix").  To ensure the text of the would-
   * be prefix and would-be suffix are included in the results, make sure the
   * first index is 0 and the last index is |aString.length|.
   *
   * @params aString
   * @params aIndexes
   * @return
   *        An array of strings.
   */
  splitAt: function CT_SplitAt(aString, aIndexes)
  {
    let result = [];
    // NB: |i = 1| so |i - 1| works on the first iteration.
    for (let i = 1, n = aIndexes.length; i < n; ++i) {
      result.push(aString.substring(aIndexes[i - 1], aIndexes[i]));
    }

    return result;
  }
}
