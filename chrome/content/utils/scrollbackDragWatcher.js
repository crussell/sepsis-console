/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Orchestrates drag and drop for the console scrollback.
 *
 * When the operator begins dragging a draggable value (e.g., a string), the
 * scrollback document's body gets its "dragover" attribute set to "true".
 * During a drag, while over any "outer"-class elements, those elements also
 * get their "dragover" attribute set (until the dragged object is dropped or
 * the cursor is moved over another node during the drag).  When the drag
 * finishes, the body's "dragover" attribute also disappears.
 *
 * When the drop occurs, the drop responder (given to |ScrollbackDragWatcher|
 * instances during construction) has its |valueDroppedOnID| method called
 * with the dropped value and drop target's |textContent|.  "outer"-class
 * elements are the only valid drop targets.  Their text content is expected
 * to be a valid identifier name.  The |ConsoleWriter| instance is actually
 * what controls the creation of outers (and inners, for that matter).
 *
 * |ScrollbackDragWatcher| uses the "application/vnd.mozilla.sepsis.jsval"
 * type.  We also accept objects with the "application/x-moz-node" type.
 * Others are ignored.
 *
 * Highlighting is not a script-powered effect; besides the "dragover"
 * attributes that we set here, highlighting is completely controlled by CSS.
 * See |ConsoleWriter.prototype.writeInput| for more details.
 *
 * The value associated with the drag is also controlled by the
 * |ConsoleWriter|, when it creates the draggables.
 *
 * So again, briefly, the |ConsoleWriter| controls the creation of draggables,
 * their associated value, and wrapping drop targets in "outer"-class
 * elements; |ScrollbackDragWatcher| instances negotiate the drag-and-drop by
 * setting the "dragover" attribute as appropriate; and the drop itself is
 * effected by the responder's |valueDroppedOnID| method.
 *
 * @see utils/dropResponder.js
 * @see utils/consoleWriter.js
 * @see contentaccessible/skin/classic/scrollback.css
 *
 * TODO
 * - Security errors when dragging some content object to a console with a
 *   different... principle?
 * - When dragging fast enough to some window not managed by the host app, the
 *   scrollback stays locked in "dragover" mode, both during drag and after
 *   the drop occurs.
 */

/**
 * @param aConsoleWriter
 *        |ConsoleWriter| instance.
 * @param aResponder
 *        Any object with a |valueDroppedOnID| method.
 * @see utils/consoleWriter.js
 * @see integration/inspector/viewer/viewerConsoleWriter.js
 * @see utils/dropResponder.js
 */
function ScrollbackDragWatcher(aConsoleWriter, aResponder)
{
  this.mConsoleWriter = aConsoleWriter;
  this.mScrollback = aConsoleWriter.scrollback;
  this.mResponder = aResponder;

  let doc = this.mScrollback.contentDocument;
  doc.addEventListener("dragstart", this, false);
  doc.addEventListener("dragenter", this, false);
  doc.addEventListener("dragover", this, false);
  doc.addEventListener("dragleave", this, false);
  doc.addEventListener("drop", this, false);
}

let (proto = ScrollbackDragWatcher.prototype) {
  proto.mConsoleWriter = null;
  proto.mResponder = null;

  proto.NODE_FLAVOR = ScrollbackDragWatcher.NODE_FLAVOR =
    "application/x-moz-node";
  proto.JSVAL_FLAVOR = ScrollbackDragWatcher.JSVAL_FLAVOR =
    "application/vnd.mozilla.sepsis.jsval";

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function SDW_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "dragstart":
        this._onDragStart(aEvent);
      break;
      case "dragenter":
      case "dragover":
        this._onDragEnterOrOver(aEvent);
      break;
      case "dragleave":
        this._onDragLeave(aEvent);
      break;
      case "drop":
        this._onDrop(aEvent);
      break;
      default:
        Cu.reportError("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param aEvent
   *        "dragstart" nsIDOMEvent
   */
  proto._onDragStart = function SDW_OnDragStart(aEvent)
  {
    // XXX onlyChromeDrop
    if (!PrefManager.getPref("draganddrop.enabled")) {
      return;
    }

    // NB: |aEvent.target.x_value| is untrusted.
    let _x_value =
      this.mConsoleWriter.getDraggableValue(aEvent.target);
    let transfer = aEvent.dataTransfer;
    transfer.mozSetDataAt(this.JSVAL_FLAVOR, _x_value, 0);
    if (_x_value instanceof Ci.nsIDOMNode) {
      transfer.mozSetDataAt(this.NODE_FLAVOR, _x_value, 0);
    }

    // We can't just rely on dragenter, because no dragenter will occur when
    // the drag originates from within the scrollback document.
    this.mScrollback.contentDocument.body.setAttribute("dragover", "true");
    this.mScrollback.scrollOnOutput = false;
  };

  /**
   * Given a node, update the "dragover" attribute on its most immediate
   * "outer"-class element.
   *
   * @param aNode
   *        The node the cursor is over during a drag.
   * @param aRemove [optional]
   *        True if the "dragover" attribute should be removed, rather than
   *        set.  Defaults to false.
   * @return
   *        True iff the dragged thing is droppable here.  Meaningless if
   *        |aRemove| is true.
   */
  proto._updateDragover = function SDW_UpdateDrageover(aNode, aRemove)
  {
    let element = this._getOuter(aNode);
    if (!element) {
      return false;
    }
    
    if (aRemove) {
      element.removeAttribute("dragover");
    }
    else {
      element.setAttribute("dragover", "true");
    }

    return true;
  };

  /**
   * @param aNode
   * @return
   *        The node's most immediate "outer"-class ancestor element, or null
   *        if none.
   */
  proto._getOuter = function SDW_GetOuter(aNode)
  {
    let node = aNode;
    while (node) {
      if (node instanceof Ci.nsIDOMElement &&
          node.classList.contains("outer")) {
        return node;
      }

      node = node.parentNode;
    }

    return null;
  };

  /**
   * @param aEvent
   *        "dragenter" or "dragover" nsIDOMEvent
   */
  proto._onDragEnterOrOver = function SDW_OnDragEnterOrOver(aEvent)
  {
    if (!PrefManager.getPref("draganddrop.enabled")) {
      return;
    }

    let types = aEvent.dataTransfer.types;
    if (!types.contains(this.JSVAL_FLAVOR) &&
        !types.contains(this.NODE_FLAVOR)) {
      return;
    }

    // Freeze automatic scrolling while the drag is ongoing, for security.
    this.mScrollback.scrollOnOutput = false;

    this.mScrollback.contentDocument.body.setAttribute("dragover", "true");
    let isDroppable = this._updateDragover(aEvent.target);
    if (isDroppable) {
      aEvent.preventDefault();
    }
  };

  /**
   * @param aEvent
   *        "dragleave" nsIDOMEvent
   */
  proto._onDragLeave = function SDW_OnDragLeave(aEvent)
  {
    this._removeDragover(aEvent.relatedTarget);
  };

  /**
   * To be called when the scrollback body element or an .outer with an
   * existing "dragover" attribute needs to have it removed.
   *
   * @param aEnteredNode [optional]
   *        Iff from a "dragleave" event, the node the drag has left to, or
   *        null.
   */
  proto._removeDragover = function SDW_RemoveDragover(aEnteredNode)
  {
    // Two cases:
    // 1. The drag is over some other element in our document, so we need
    //    to make sure any previously highlighted outer is no longer
    //    highlighted.
    // 2. Drag ended or has left the scrollback document for some element
    //    outside it.  The scrollback document should revert to the way it
    //    looks where there's no drag going on anywhere.

    // assert(scrollbackDoc.querySelector(".outer[dragover]").length <= 1);
    let scrollbackDoc = this.mScrollback.contentDocument;
    let node = scrollbackDoc.querySelector(".outer[dragover]");

    // Further handling for case 2 (if we detect that it is case 2).
    if (!aEnteredNode || aEnteredNode.ownerDocument != scrollbackDoc) {
      scrollbackDoc.body.removeAttribute("dragover");
      // See above.
      this.mScrollback.scrollOnOutput = true;
      this.mScrollback.scrollToBottom();
    }

    if (node) {
      this._updateDragover(node, true);
    }
  };

  /**
   * Handle the drop by delegating to the responder's |valueDroppedOnID|
   * method.
   *
   * NB: |aEvent.dataTransfer|'s data is untrusted.
   *
   * @param aEvent
   *        A "drop" |nsIDOMEvent|.
   */
  proto._onDrop = function SDW_OnDrop(aEvent)
  {
    this._removeDragover();

    let types = aEvent.dataTransfer.types;
      // NB: |_x_value| is untrusted.
    let _x_value;
    if (types.contains(this.JSVAL_FLAVOR)) {
      _x_value = aEvent.dataTransfer.mozGetDataAt(this.JSVAL_FLAVOR, 0);
    }
    else if (types.contains(this.NODE_FLAVOR)) {
      _x_value = aEvent.dataTransfer.mozGetDataAt(this.NODE_FLAVOR, 0);
    }
    else {
      Cu.reportError("wat.  Types:\n" + [].join.call(types, "\n"));
    }

    let name = this._getOuter(aEvent.target).textContent;
    this.mResponder.valueDroppedOnID(_x_value, name);
  };
}
