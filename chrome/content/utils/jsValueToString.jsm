/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Utility function for getting string representations of a JS value.
 *
 * (NB: This file originates from
 *   https://hg.mozilla.org/users/sevenspade_gmail.com/jsvaluetostring/
 *
 * Consider changing the upstream source instead of local copies.)
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "jsValueToString" ];

const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-console/content/utils/stringParser.jsm");

//////////////////////////////////////////////////////////////////////////////

/**
 * Obtain a good string representation of the given value.  (E.g., for
 * printing in the console).
 *
 * @param aValue
 *        The value to obtain a string representation of.
 * @return
 *        A primitive string representing the object.
 */
function jsValueToString(aValue)
{
  let str;

  if (typeof(aValue) == "string") {
    str = StringParser.uneval(aValue);
  }
  else {
    try {
      str = String(aValue);
    }
    catch (ex) {
      // This method can also throw, but only if |aValue| is null or
      // undefined.  That's not a problem, though; we shouldn't be able to
      // reach this point for those types, because String() can handle them
      // just fine.
      str = Object.prototype.toString.call(aValue);
    }
  }

  return str;
}
