<?xml version="1.0"?>
<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this file,
   - You can obtain one at http://mozilla.org/MPL/2.0/. -->

<bindings id="consoleInputBindings"
          xmlns="http://www.mozilla.org/xbl"
          xmlns:xul="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul">
  <binding id="consoleInput"
           extends="chrome://global/content/bindings/textbox.xml#textarea">
    <implementation>
      <field name="mMultiLineMode">false</field>

      <!-- Input history recall stacks. -->
      <field name="mHistoryEntries">[]</field>
      <!--
      // When the user navigates to a history entry, edits the recalled value,
      // navigates to another entry, and then back to the edited one, we want
      // it to reflect the post-edit value. (cf bash)  We don't want it to
      // overwrite the entries when they're edited, though.  So we keep two
      // stacks.  mHistoryEntries maintains the "true" input history.  We keep
      // any post-edit values in mEditedEntries.  They're "symmetric", so, for
      // an entry at some index in one stack, its corresponding entry is found
      // at the same index in the other stack.  Mostly.  (See the
      // mHistoryIndex comment for the case where this isn't true.)
      //
      // We only ever store anything in mEditedEntries if it should differ
      // from its corresponding entry in mHistoryEntries (i.e., it was
      // edited).  All other elements are uninitialized, which means "refer to
      // the unmolested entry in mHistoryEntries instead".  NB: That really
      // does mean "uninitialized", not "initialized with |undefined|".
      -->
      <field name="mEditedEntries">[]</field>

      <!--
      // The input history can be navigated (e.g., with the arrow keys).
      // mHistoryIndex tracks our current position in the history stack.  The
      // stack is oriented so the bottom is at index 0.
      //
      // When the index is one past the topmost stack entry (i.e., the same as
      // the array length of mHistoryEntries), it means that we are currently
      // at a new prompt, rather than any previous entry.  In that case, this
      // won't be a valid index into mHistoryEntries, but it will be valid for
      // mEditedEntries, where we will preserve any value typed at a new
      // prompt.  This will allow the user to type something at a new prompt,
      // navigate various entries, then navigate back out of the history stack
      // to the value originally entered.
      -->
      <field name="mHistoryIndex">0</field>

      <!--
      // Some terminology:
      //
      // New prompt
      //   We aren't showing some past input value that we've recalled from
      //   the history stack (or some edited form of it).  NB: "New" here
      //   doesn't mean "empty".
      //
      //   The following are all instances of a new prompt:
      //     The containing document has loaded for the first time, or the
      //     bound element was just added to the document.  The input is empty
      //     or showing a placeholder value.
      //
      //     The owner document has loaded for the first time, and the user
      //     has typed something in the input but has not yet hit enter.
      //
      //     The user may or may not yet have typed something.  Then they hit
      //     the up arrow to recall some previous accepted input value, then
      //     the down arrow to arrive at whatever they had typed before going
      //     up in the history (or a blank input, if they hadn't typed
      //     anything).
      -->

      <!-- See the comments for _handleEnterKeypress. -->
      <field name="mRisingCommandListener"><![CDATA[
        ({
          mInput: null,

          init: function CI_RCL_Init(aInput)
          {
            this.mInput = aInput;
          },

          handleEvent: function CI_RCL_HandleEvent(aEvent)
          {
            // Only accept the input value when we're supposed to.
            if (!aEvent.defaultPrevented || this.mInput.clearOnPrevention) {
              this.mInput.acceptInput();
            }
          }
        })
      ]]></field>

      <field name="mHistoryNavSuspended">false</field>
      <field name="mEnterSuspended">false</field>

      <constructor><![CDATA[
        this.mRisingCommandListener.init(this);
      ]]></constructor>

      <destructor><![CDATA[
        this.removeEventListener("command", this.mRisingCommandListener,
                                 false);
      ]]></destructor>

      <property name="historyNavSuspended"
                onget="return this.mHistoryNavSuspended"
                onset="return this.mHistoryNavSuspended = val;"/>

      <property name="enterSuspended"
                onget="return this.mEnterSuspended"
                onset="return this.mEnterSuspended = val;"/>

      <!--
      // When the user enters some text and hits Enter, we fire off this
      // control's command.  By default (after the consumer's command listener
      // is processed), we will clear the input.  The listener can use
      // preventDefault() on the command event to prevent the input from being
      // cleared.
      //
      // If clearOnPrevention is true, the input will be cleared regardless of
      // whether preventDefault() is called.
      -->
      <property name="clearOnPrevention">
        <getter><![CDATA[
          return !!this.getAttribute("clearonprevention");
        ]]></getter>

        <setter><![CDATA[
          if (val) {
            this.setAttribute("clearonprevention", "true");
          }
          else {
            this.removeAttribute("clearonprevention");
          }
          return val;
        ]]></setter>
      </property>

      <property name="multiLineMode"
                readonly="true"
                onget="return this.mMultiLineMode;"/>

      <method name="acceptInput">
        <body><![CDATA[
          // Clear input and add to history, but don't bother adding the input
          // value to the history if it's an empty string or if it's identical
          // to the last entry that was added.
          if (this.value &&
              (!this.mHistoryEntries.length ||
               this.mHistoryEntries[this.mHistoryEntries.length - 1] !=
                 this.value)) {

            this.mHistoryEntries.push(this.value);

            // Forget any edited entries.
            // XXX Well, holy shit, it turns out bash doesn't work like this;
            // edits *are* permanent history overwrites.  I like it like this,
            // though.  Do a compatibility pref.  --crussell
            this.mEditedEntries = [];
          }
          this.mHistoryIndex = this.mHistoryEntries.length;

          // Clear the input and reset the editor's undo/redo stack.
          this.reset();

          this._dispatchInputReadyEvent();
        ]]></body>
      </method>

      <method name="toggleMultiLineMode">
        <body><![CDATA[
          // TODO Set the height to max(currentHeight, multiLineDefault)
          this.mMultiLineMode = !this.mMultiLineMode;
        ]]></body>
      </method>

      <method name="_handleEnterKeypress">
        <parameter name="aEvent"/>
        <body><![CDATA[
          // Shift + Enter inserts a newline, and in multi-line mode the user
          // must press Ctrl/Cmd + Enter to get the command handler to fire.
          // Someone may also be using |beginEnterSuspendBatch| to pre-empt
          // us.
          if (aEvent.shiftKey || (this.mMultiLineMode && !aEvent.accelKey) ||
              this.mEnterSuspended) {
            return;
          }

          // We're about to cause a command event to be fired, which will
          // allow, e.g., a consumer's oncommand listener to perform some
          // action.  Consumers can use preventDefault in order to signal to
          // us not to accept the input, which means we need to perform the
          // check as part of the event flow in a listener of our own.
          //
          // We can't just use an XBL handler, though, because they get
          // attached early on, and we still want to work even if the consumer
          // used addEventListener (or something else instead of oncommand),
          // which will cause its listener(s) to fire later than an XBL
          // handler.  So we have a separate listener, mRisingCommandListener,
          // that we remove and re-add so that it will be at the end of the
          // listener sequence, triggering just before bubbling phase begins.
          this.removeEventListener("command", this.mRisingCommandListener,
                                   false);
          this.addEventListener("command", this.mRisingCommandListener, false);
          this.doCommand()

          // Don't let the editor insert a newline.
          aEvent.preventDefault();

          // XXX Check for overflow and resize appropriately.  (We'll
          // literally want to do this in a separate overflow handler, so that
          // it works for paste.  For now, consumers will have to handle this
          // until it's obvious how this can be achieved in a clean way.)
        ]]></body>
      </method>

      <method name="handleHistoryNavKeypress">
        <parameter name="aEvent"/>
        <body><![CDATA[
          if (this.mHistoryNavSuspended) {
            return;
          }

          switch (aEvent.keyCode) {
            case aEvent.DOM_VK_UP:
              if (this.mHistoryIndex <= 0) {
                return;
              }

              this._saveEditsForHistoryNavUp();
              --this.mHistoryIndex;
            break;
            case aEvent.DOM_VK_DOWN:
              if (this.mHistoryIndex >= this.mHistoryEntries.length) {
                return;
              }

              this._saveEditsForHistoryNavDown();
              ++this.mHistoryIndex;
            break;
            default:
              return;
          }

          // We can't just do a shorthand test for falsy values, because we
          // want this to work even if the edited entry was an empty string.
          this.value = (this.mHistoryIndex in this.mEditedEntries) ?
            this.mEditedEntries[this.mHistoryIndex] :
            this.mHistoryEntries[this.mHistoryIndex];
          // Undo/redo shouldn't recall the values of other entries.
          // XXX Should we preserve the undo/redo stack with the edited entry,
          // in case they return to it?
          this.inputField.editor.transactionManager.clear();
          this._dispatchInputEvent();

          // Don't let the editor translate this event into cursor movement.
          aEvent.preventDefault();
        ]]></body>
      </method>

      <method name="_saveEditsForHistoryNavUp">
        <body><![CDATA[
          // Case 1: We're at a new prompt.  We're going to recall the last
          // input value that was accepted.  We need to stuff the current
          // input value into a new entry at the top of the stack of edited
          // entries.  At this point, the length of mEditedEntries will become
          // one longer than mHistoryEntries.
          //
          // Case 2: We're somewhere in the history stack.  The user has
          // edited the recalled input value to differ from the one in the
          // history, and we're about to recall the entry previous to that
          // one.  We need to set the entry at the current position in the
          // edited stack to our current input value.
          if (this.mHistoryIndex == this.mHistoryEntries.length ||
              this.value != this.mHistoryEntries[this.mHistoryIndex]) {
            this.mEditedEntries[this.mHistoryIndex] = this.value;
          }
          else {
            // Stupid Edge Case 3: We're somewhere in the history stack.  At
            // some point since the last input value was accepted, the user
            // has navigated to the current position in the stack, edited it,
            // navigated further up or down in the history, then back to the
            // edited entry.  They've now further edited the entry to be
            // identical to its value before the previous edit.  I.e., it now
            // matches its corresponding entry in mHistoryEntries.
            //
            // We *could* pull this out and work a similar check into the
            // conditional above, allowing the edited entry to get overwritten
            // with a value identical to its corresponding entry in the
            // history stack.  Everything would even work thaty way.  Better
            // to call special attention to this case, though, since it
            // doesn't immediately come to mind.
            if (this.mHistoryIndex in this.mEditedEntries) {
              delete this.mEditedEntries[this.mHistoryIndex];
            }
          }
        ]]></body>
      </method>

      <method name="_saveEditsForHistoryNavDown">
        <body><![CDATA[
          // The cases are similar to case 2 and 3 above.
          if (this.value != this.mHistoryEntries[this.mHistoryIndex]) {
            this.mEditedEntries[this.mHistoryIndex] = this.value;
          }
          else if (this.mHistoryIndex in this.mEditedEntries) {
            delete this.mEditedEntries[this.mHistoryIndex];
          }
        ]]></body>
      </method>

      <method name="_dispatchInputEvent">
        <body><![CDATA[
          var doc = this.ownerDocument;
          var event = doc.createEvent("Event");
          event.initEvent("input", true, false);
          this.dispatchEvent(event);
        ]]></body>
      </method>

      <method name="_dispatchInputReadyEvent">
        <body><![CDATA[
          // XXX Do we want to dispatch this event the first time the input is
          // available (e.g., when the containing document is loaded or when
          // the bound element is inserted into the document)?  The name kind
          // of implies that we should, but right now we don't.
          var doc = this.ownerDocument;
          var event = doc.createEvent("UIEvent");
          event.initUIEvent("inputready", true, false, doc.defaultView, 0);
          this.dispatchEvent(event);
        ]]></body>
      </method>
    </implementation>

    <handlers>
      <handler event="keypress" keycode="VK_RETURN"><![CDATA[
        this._handleEnterKeypress(event);
      ]]></handler>

      <handler event="keypress" keycode="VK_RETURN"
               modifiers="accel"><![CDATA[
        event.accelKey = true;
        this._handleEnterKeypress(event);
      ]]></handler>

      <handler event="keypress" keycode="VK_RETURN"
               modifiers="shift,accel"><![CDATA[
        this.toggleMultiLineMode();
        event.preventDefault();
      ]]></handler>

      <handler event="keypress" keycode="VK_UP"><![CDATA[
        // XXX This doesn't play nice with using the keyboard for cursor
        // movement with multi-line text values.
        this.handleHistoryNavKeypress(event);
      ]]></handler>

      <handler event="keypress" keycode="VK_DOWN"><![CDATA[
        this.handleHistoryNavKeypress(event);
      ]]></handler>
    </handlers>
  </binding>
</bindings>
