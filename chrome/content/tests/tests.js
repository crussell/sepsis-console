var Test = {
  is: function T_Is(a, b, msg)
  {
    if (a != b) {
      let err = "got: " + a + "\nexpected: " + b;
      if (msg) {
        err += "\n" + msg;
      }

      let error = new Error(err);
      Components.utils.reportError(error.stack);
      throw error;
    }
  },

  ok: function T_Ok(expression, msg)
  {
    if (!expression) {
      let err = "expected true expression";
      if (msg) {
        err += "\n" + msg;
      }
      let error = new Error(err);
      Components.utils.reportError(error.stack);
      throw error;
    }
  },

  showResults: function T_ShowResults()
  {
    alert("no tests failed");
  }
};
