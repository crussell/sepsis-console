let (proto = ConsoleAutoCompleteSearch.prototype) {
  // Test with and without leading backslash
  let testGuess = function AC_T_TestGuess(aSequence, aCharacter, aResult)
  {
    Test.is(proto.guessSequence.call(proto, aSequence, aCharacter), aResult);
    aSequence = "\\" + aSequence;
    aResult = "\\" + aResult;
    Test.is(proto.guessSequence.call(proto, aSequence, aCharacter), aResult);
  };

  // Start with the examples from the method description.
  testGuess("", "m", "m");
  testGuess("u00", "m", "u006d");
  testGuess("x", "m", "x6d");
  testGuess("1", "m", "155");

  // Test the "idempotent-ish"-ness property that CACS_GetFullSequences relies
  // on.
  testGuess("m", "m", "m");
  testGuess("u006d", "m", "u006d");
  testGuess("x6d", "m", "x6d");
  testGuess("155", "m", "155");

  testGuess("", "b", "u0062");
  testGuess("", "f", "u0066");
  testGuess("", "n", "u006e");
  testGuess("", "r", "u0072");
  testGuess("", "t", "u0074");
  testGuess("", "v", "u0076");

  testGuess("", "\\", "\\");
  testGuess("", "'", "'");
  testGuess("", "\"", "\"");

  // "Idempotent-ish"-ness for Ā, the first code point requiring a width of
  // > 7 bits.
  testGuess("\u0100", "\u0100", "\u0100");

  let (caughtError = false) {
    try {
      testGuess("4", "\u0100");
    }
    catch (ex) {
      caughtError = true;
    }

    Test.ok(caughtError);
  }

  // Octal
  testGuess("0", "\0", "0");
  testGuess("1", "\1", "1");
  testGuess("2", "\2", "2");
  testGuess("3", "\3", "3");
  testGuess("4", "\4", "4");
  testGuess("5", "\5", "5");
  testGuess("6", "\6", "6");
  testGuess("7", "\7", "7");
  testGuess("1", "\10", "10");
  testGuess("1", "\11", "11");
  testGuess("1", "\12", "12");
  testGuess("1", "\13", "13");
  testGuess("1", "\14", "14");
  testGuess("1", "\15", "15");
  testGuess("1", "\16", "16");
  testGuess("1", "\17", "17");
  testGuess("2", "\20", "20");
  testGuess("2", "\21", "21");
  testGuess("2", "\22", "22");
  testGuess("2", "\23", "23");
  testGuess("2", "\24", "24");
  testGuess("2", "\25", "25");
  testGuess("2", "\26", "26");
  testGuess("2", "\27", "27");
  testGuess("3", "\30", "30");
  testGuess("3", "\31", "31");
  testGuess("3", "\32", "32");
  testGuess("3", "\33", "33");
  testGuess("3", "\34", "34");
  testGuess("3", "\35", "35");
  testGuess("3", "\36", "36");
  testGuess("3", "\37", "37");
  testGuess("4", "\40", "40");
  testGuess("4", "\41", "41");
  testGuess("4", "\42", "42");
  testGuess("4", "\43", "43");
  testGuess("4", "\44", "44");
  testGuess("4", "\45", "45");
  testGuess("4", "\46", "46");
  testGuess("4", "\47", "47");
  testGuess("5", "\50", "50");
  testGuess("5", "\51", "51");
  testGuess("5", "\52", "52");
  testGuess("5", "\53", "53");
  testGuess("5", "\54", "54");
  testGuess("5", "\55", "55");
  testGuess("5", "\56", "56");
  testGuess("5", "\57", "57");
  testGuess("6", "\60", "60");
  testGuess("6", "\61", "61");
  testGuess("6", "\62", "62");
  testGuess("6", "\63", "63");
  testGuess("6", "\64", "64");
  testGuess("6", "\65", "65");
  testGuess("6", "\66", "66");
  testGuess("6", "\67", "67");
  testGuess("7", "\70", "70");
  testGuess("7", "\71", "71");
  testGuess("7", "\72", "72");
  testGuess("7", "\73", "73");
  testGuess("7", "\74", "74");
  testGuess("7", "\75", "75");
  testGuess("7", "\76", "76");
  testGuess("7", "\77", "77");
  testGuess("1", "\100", "100");
  testGuess("1", "\101", "101");
  testGuess("1", "\102", "102");
  testGuess("1", "\103", "103");
  testGuess("1", "\104", "104");
  testGuess("1", "\105", "105");
  testGuess("1", "\106", "106");
  testGuess("1", "\107", "107");
  testGuess("1", "\110", "110");
  testGuess("1", "\111", "111");
  testGuess("1", "\112", "112");
  testGuess("1", "\113", "113");
  testGuess("1", "\114", "114");
  testGuess("1", "\115", "115");
  testGuess("1", "\116", "116");
  testGuess("1", "\117", "117");
  testGuess("1", "\120", "120");
  testGuess("1", "\121", "121");
  testGuess("1", "\122", "122");
  testGuess("1", "\123", "123");
  testGuess("1", "\124", "124");
  testGuess("1", "\125", "125");
  testGuess("1", "\126", "126");
  testGuess("1", "\127", "127");
  testGuess("1", "\130", "130");
  testGuess("1", "\131", "131");
  testGuess("1", "\132", "132");
  testGuess("1", "\133", "133");
  testGuess("1", "\134", "134");
  testGuess("1", "\135", "135");
  testGuess("1", "\136", "136");
  testGuess("1", "\137", "137");
  testGuess("1", "\140", "140");
  testGuess("1", "\141", "141");
  testGuess("1", "\142", "142");
  testGuess("1", "\143", "143");
  testGuess("1", "\144", "144");
  testGuess("1", "\145", "145");
  testGuess("1", "\146", "146");
  testGuess("1", "\147", "147");
  testGuess("1", "\150", "150");
  testGuess("1", "\151", "151");
  testGuess("1", "\152", "152");
  testGuess("1", "\153", "153");
  testGuess("1", "\154", "154");
  testGuess("1", "\155", "155");
  testGuess("1", "\156", "156");
  testGuess("1", "\157", "157");
  testGuess("1", "\160", "160");
  testGuess("1", "\161", "161");
  testGuess("1", "\162", "162");
  testGuess("1", "\163", "163");
  testGuess("1", "\164", "164");
  testGuess("1", "\165", "165");
  testGuess("1", "\166", "166");
  testGuess("1", "\167", "167");
  testGuess("1", "\170", "170");
  testGuess("1", "\171", "171");
  testGuess("1", "\172", "172");
  testGuess("1", "\173", "173");
  testGuess("1", "\174", "174");
  testGuess("1", "\175", "175");
  testGuess("1", "\176", "176");
  testGuess("1", "\177", "177");
  testGuess("2", "\200", "200");
  testGuess("2", "\201", "201");
  testGuess("2", "\202", "202");
  testGuess("2", "\203", "203");
  testGuess("2", "\204", "204");
  testGuess("2", "\205", "205");
  testGuess("2", "\206", "206");
  testGuess("2", "\207", "207");
  testGuess("2", "\210", "210");
  testGuess("2", "\211", "211");
  testGuess("2", "\212", "212");
  testGuess("2", "\213", "213");
  testGuess("2", "\214", "214");
  testGuess("2", "\215", "215");
  testGuess("2", "\216", "216");
  testGuess("2", "\217", "217");
  testGuess("2", "\220", "220");
  testGuess("2", "\221", "221");
  testGuess("2", "\222", "222");
  testGuess("2", "\223", "223");
  testGuess("2", "\224", "224");
  testGuess("2", "\225", "225");
  testGuess("2", "\226", "226");
  testGuess("2", "\227", "227");
  testGuess("2", "\230", "230");
  testGuess("2", "\231", "231");
  testGuess("2", "\232", "232");
  testGuess("2", "\233", "233");
  testGuess("2", "\234", "234");
  testGuess("2", "\235", "235");
  testGuess("2", "\236", "236");
  testGuess("2", "\237", "237");
  testGuess("2", "\240", "240");
  testGuess("2", "\241", "241");
  testGuess("2", "\242", "242");
  testGuess("2", "\243", "243");
  testGuess("2", "\244", "244");
  testGuess("2", "\245", "245");
  testGuess("2", "\246", "246");
  testGuess("2", "\247", "247");
  testGuess("2", "\250", "250");
  testGuess("2", "\251", "251");
  testGuess("2", "\252", "252");
  testGuess("2", "\253", "253");
  testGuess("2", "\254", "254");
  testGuess("2", "\255", "255");
  testGuess("2", "\256", "256");
  testGuess("2", "\257", "257");
  testGuess("2", "\260", "260");
  testGuess("2", "\261", "261");
  testGuess("2", "\262", "262");
  testGuess("2", "\263", "263");
  testGuess("2", "\264", "264");
  testGuess("2", "\265", "265");
  testGuess("2", "\266", "266");
  testGuess("2", "\267", "267");
  testGuess("2", "\270", "270");
  testGuess("2", "\271", "271");
  testGuess("2", "\272", "272");
  testGuess("2", "\273", "273");
  testGuess("2", "\274", "274");
  testGuess("2", "\275", "275");
  testGuess("2", "\276", "276");
  testGuess("2", "\277", "277");
  testGuess("3", "\300", "300");
  testGuess("3", "\301", "301");
  testGuess("3", "\302", "302");
  testGuess("3", "\303", "303");
  testGuess("3", "\304", "304");
  testGuess("3", "\305", "305");
  testGuess("3", "\306", "306");
  testGuess("3", "\307", "307");
  testGuess("3", "\310", "310");
  testGuess("3", "\311", "311");
  testGuess("3", "\312", "312");
  testGuess("3", "\313", "313");
  testGuess("3", "\314", "314");
  testGuess("3", "\315", "315");
  testGuess("3", "\316", "316");
  testGuess("3", "\317", "317");
  testGuess("3", "\320", "320");
  testGuess("3", "\321", "321");
  testGuess("3", "\322", "322");
  testGuess("3", "\323", "323");
  testGuess("3", "\324", "324");
  testGuess("3", "\325", "325");
  testGuess("3", "\326", "326");
  testGuess("3", "\327", "327");
  testGuess("3", "\330", "330");
  testGuess("3", "\331", "331");
  testGuess("3", "\332", "332");
  testGuess("3", "\333", "333");
  testGuess("3", "\334", "334");
  testGuess("3", "\335", "335");
  testGuess("3", "\336", "336");
  testGuess("3", "\337", "337");
  testGuess("3", "\340", "340");
  testGuess("3", "\341", "341");
  testGuess("3", "\342", "342");
  testGuess("3", "\343", "343");
  testGuess("3", "\344", "344");
  testGuess("3", "\345", "345");
  testGuess("3", "\346", "346");
  testGuess("3", "\347", "347");
  testGuess("3", "\350", "350");
  testGuess("3", "\351", "351");
  testGuess("3", "\352", "352");
  testGuess("3", "\353", "353");
  testGuess("3", "\354", "354");
  testGuess("3", "\355", "355");
  testGuess("3", "\356", "356");
  testGuess("3", "\357", "357");
  testGuess("3", "\360", "360");
  testGuess("3", "\361", "361");
  testGuess("3", "\362", "362");
  testGuess("3", "\363", "363");
  testGuess("3", "\364", "364");
  testGuess("3", "\365", "365");
  testGuess("3", "\366", "366");
  testGuess("3", "\367", "367");
  testGuess("3", "\370", "370");
  testGuess("3", "\371", "371");
  testGuess("3", "\372", "372");
  testGuess("3", "\373", "373");
  testGuess("3", "\374", "374");
  testGuess("3", "\375", "375");
  testGuess("3", "\376", "376");
  testGuess("3", "\377", "377");

  // Octal with leading zero.
  testGuess("0", "\0", "0");
  testGuess("0", "\1", "01");
  testGuess("0", "\2", "02");
  testGuess("0", "\3", "03");
  testGuess("0", "\4", "04");
  testGuess("0", "\5", "05");
  testGuess("0", "\6", "06");
  testGuess("0", "\7", "07");
  testGuess("0", "\10", "010");
  testGuess("0", "\11", "011");
  testGuess("0", "\12", "012");
  testGuess("0", "\13", "013");
  testGuess("0", "\14", "014");
  testGuess("0", "\15", "015");
  testGuess("0", "\16", "016");
  testGuess("0", "\17", "017");
  testGuess("0", "\20", "020");
  testGuess("0", "\21", "021");
  testGuess("0", "\22", "022");
  testGuess("0", "\23", "023");
  testGuess("0", "\24", "024");
  testGuess("0", "\25", "025");
  testGuess("0", "\26", "026");
  testGuess("0", "\27", "027");
  testGuess("0", "\30", "030");
  testGuess("0", "\31", "031");
  testGuess("0", "\32", "032");
  testGuess("0", "\33", "033");
  testGuess("0", "\34", "034");
  testGuess("0", "\35", "035");
  testGuess("0", "\36", "036");
  testGuess("0", "\37", "037");
  testGuess("0", "\40", "040");
  testGuess("0", "\41", "041");
  testGuess("0", "\42", "042");
  testGuess("0", "\43", "043");
  testGuess("0", "\44", "044");
  testGuess("0", "\45", "045");
  testGuess("0", "\46", "046");
  testGuess("0", "\47", "047");
  testGuess("0", "\50", "050");
  testGuess("0", "\51", "051");
  testGuess("0", "\52", "052");
  testGuess("0", "\53", "053");
  testGuess("0", "\54", "054");
  testGuess("0", "\55", "055");
  testGuess("0", "\56", "056");
  testGuess("0", "\57", "057");
  testGuess("0", "\60", "060");
  testGuess("0", "\61", "061");
  testGuess("0", "\62", "062");
  testGuess("0", "\63", "063");
  testGuess("0", "\64", "064");
  testGuess("0", "\65", "065");
  testGuess("0", "\66", "066");
  testGuess("0", "\67", "067");
  testGuess("0", "\70", "070");
  testGuess("0", "\71", "071");
  testGuess("0", "\72", "072");
  testGuess("0", "\73", "073");
  testGuess("0", "\74", "074");
  testGuess("0", "\75", "075");
  testGuess("0", "\76", "076");
  testGuess("0", "\77", "077");

  // Octal with two leading zeroes.
  testGuess("00", "\0", "00");
  testGuess("00", "\1", "001");
  testGuess("00", "\2", "002");
  testGuess("00", "\3", "003");
  testGuess("00", "\4", "004");
  testGuess("00", "\5", "005");
  testGuess("00", "\6", "006");
  testGuess("00", "\7", "007");

  // Three-zero octal.
  testGuess("000", "\0", "000");
}

let fakeWin = {};
let env = new ConsoleEnvironment();
let ac = new ConsoleAutoCompleteSearch(env);
let ct = CrummyTokenizer;

fakeWin.document = {
  body: {
    nodeName: "BODY",
    nodeType: 1,
    nodeValue: null
  }
};

env.sandbox.document = fakeWin.document;

let searchString = "document.body.node";
let ids = ct.splitIDs(searchString, ct.findSplitPoints(searchString).pop());
let partialID = ids.pop();
let baseObj = env.resolveObject(ids);
Test.is(baseObj, fakeWin.document.body);
let matches =
  ac.getPropertyNamesMatchingPrefix(baseObj, partialID.getValue());
Test.is(matches.length, 3);
Test.is(JSON.stringify(matches),
        JSON.stringify([ "nodeName", "nodeType", "nodeValue" ]));

ac = new ConsoleAutoCompleteSearch(env);
searchString = "'omghax'.";
ids = ct.splitIDs(searchString, ct.findSplitPoints(searchString).pop());
ids.pop();
baseObj = env.resolveObject(ids);
Test.is(typeof(baseObj), "string");
Test.is(baseObj, "omghax");

// Make sure environment removes itself as an observer for
// "console-api-log-event".
env.destroy();

Test.showResults();
