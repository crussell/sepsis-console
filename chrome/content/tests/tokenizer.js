function numberArraysEqual(a, b)
{
  Test.is(JSON.stringify(a), JSON.stringify(b));
}

function checkSourceThings(things, b)
{
  numberArraysEqual([x.getValue() for each (x in things)], b);
}

let ct = CrummyTokenizer;

let searchString = "obj.prop";
let splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [0, 3]);
checkSourceThings(ct.splitIDs(searchString, splitPoints), ["obj", "prop"]);

searchString = "obj['prop";
splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [0, 3]);
numberArraysEqual(splitPoints, ct.findSplitPoints(searchString).pop());
checkSourceThings(ct.splitIDs(searchString, splitPoints), ["obj", "prop"]);

searchString = "obj['prop'";
splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [0, 3]);
numberArraysEqual(splitPoints, ct.findSplitPoints(searchString).pop());
checkSourceThings(ct.splitIDs(searchString, splitPoints), ["obj", "prop"]);

searchString = "obj['prop']";
splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [0, 3]);
numberArraysEqual(splitPoints, ct.findSplitPoints(searchString).pop());
checkSourceThings(ct.splitIDs(searchString, splitPoints), ["obj", "prop"]);

searchString = "obj['prop'].";
splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [0, 3, 11]);
numberArraysEqual(splitPoints, ct.findSplitPoints(searchString).pop());
checkSourceThings(ct.splitIDs(searchString, splitPoints),
                  ["obj", "prop", ""]);

searchString = "\"str\".length";
splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [0, 5]);
// Don't use checkSourceThings here, because it will unescape the "str"
// SourceThing, which isn't what we want.
let (splot = ct.splitIDs(searchString, splitPoints)) {
  numberArraysEqual([splot[0].getRawSource(), splot[1].getValue()],
                    ["\"str\"", "length"]);
}

searchString = 'o.__defineSetter__("value",' +
               ' function(aVal)' +
               '{' +
               ' console.log("value set to " + aVal);' +
               ' console.log("q\'s position is " + q.cur';
splitPoints = ct.findSplitPoints(searchString).pop();
numberArraysEqual(splitPoints, [114, 115]);
checkSourceThings(ct.splitIDs(searchString, splitPoints), ["q", "cur"]);

Test.showResults();
