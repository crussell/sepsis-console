// XXX edge/base cases
let (v = null) {
  let check = function SP_T_Check(a, b)
  {
    let parser = new StringParser(a);
    Test.is(parser.value, b);
    Test.is(parser.state, StringParser.ACCEPTING_STATE);
  };

  v = "";
  check("''", v);
  check("\"\"", v);

  v = "fum";
  check("'fum'", v);
  check("\"fum\"", v);

  check("'\\\\'", "\\");

  check("'\\''", "'");

  check("'\\\"'", "\"");

  check("'\\b'", "\b");

  check("'\\f'", "\f");

  check("'\\n'", "\n");

  check("'\\r'", "\r");

  check("'\\t'", "\t");

  check("'\\v'", "\v");

  v = "\"If I don't survive, tell my wife I said, 'Hello.'\"";

  // The double-escaping can get confusing here.  The literals are:
  // "\"If I don't survive, tell my wife I said, 'Hello.'\""
  check("\"\\\"If I don't survive, tell my wife I said, 'Hello.'\\\"\"", v);
  // '"If I don\'t survive, tell my wife I said, \'Hello.\'"'
  check("'\"If I don\\'t survive, tell my wife I said, \\'Hello.\\'\"'", v);
  // "\"If I don\'t survive, tell my wife I said, \'Hello.\'\""
  check(
    "\"\\\"If I don\\'t survive, tell my wife I said, \\'Hello.\\'\\\"\"", v
  );
  // '\"If I don\'t survive, tell my wife I said, \'Hello.\'\"'
  check(
    "'\\\"If I don\\'t survive, tell my wife I said, \\'Hello.\\'\\\"'", v
  );

  v = "omghax";
  check("'\\omghax'", v);
  check("'\\omg\\hax'", v);
  check("'\\o\\m\\g\\h\\ax'", v);

  v = "z";
  check("'\\x7A'", v);
  check("'\\x7a'", v);
  check("'\\u007A'", v);
  check("'\\u007a'", v);
  check("'\\172'", v);

  v = "\0";
  check("'\\0'", v);
  check("'\\00'", v);
  check("'\\000'", v);

  check("'\\0000'", "\0" + "0");

  // These tests will also tell us whether the SpiderMonkey implementation
  // changes in ways we don't expect.  See the StringParser notes.
  // XXX More comprehensive tests here.
  v = "\0" + "8";
  check("'\\08'", v);
  Test.is("\08", v);
  check("'\\008'", v);
  Test.is("\008", v);

  check("'\\8'", "8");
  Test.is("\8", "8");

  check("'\\9'", "9");
  Test.is("\9", "9");

  v = "omghax";
  check("'omg\\\u000Ahax'", v);
  check("'omg\\\u000Dhax'", v);
  check("'omg\\\u2028hax'", v);
  check("'omg\\\u2029hax'", v);

  // These are just some tests to demonstrate/verify that StringParser doesn't
  // incorrectly double unescape, for good measure.  The concatenation for the
  // second parameter spots are simply for readability.
  check("'omg\\\\u000Ahax'", "omg" + "\\" + "u000Ahax");
  check("'omg\\\\u000Dhax'", "omg" + "\\" + "u000Dhax");
  check("'omg\\\\u2028hax'", "omg" + "\\" + "u2028hax");
  check("'omg\\\\u2029hax'", "omg" + "\\" + "u2029hax");

  check("'omg\\\\x0Ahax'", "omg" + "\\" + "x0Ahax");
  check("'omg\\\\x0Dhax'", "omg" + "\\" + "x0Dhax");

  check("'omg\\\\12hax'", "omg" + "\\" + "12hax");
  check("'omg\\\\15hax'", "omg" + "\\" + "15hax");

  check("'omg\\\\012hax'", "omg" + "\\" + "012hax");
  check("'omg\\\\015hax'", "omg" + "\\" + "015hax");
}

let (v = null) {
  let checkPartial =
    function SP_T_CheckPartial(aLiteral, aKnown, aUnterminatedSequence)
  {
    var p = new StringParser(aLiteral);
    Test.is(p.getKnownValue(), aKnown);
    Test.is(p.unterminatedSequence, aUnterminatedSequence);
  };

  checkPartial("'omg\\", "omg", "");
  checkPartial("'omg\\u", "omg", "u");
  checkPartial("'omg\\u0", "omg", "u0");
  checkPartial("'omg\\u00", "omg", "u00");
  checkPartial("'omg\\u002", "omg", "u002");
  checkPartial("'omg\\u0020", "omg ", null);
  checkPartial("'omg\\u0020hax", "omg hax", null);

  checkPartial("'", "", null);
  checkPartial("'\\", "", "");
  checkPartial("'\\x", "", "x");
  checkPartial("'\\x3", "", "x3");
  checkPartial("'\\x37", "7", null);

  checkPartial("'\\", "", "");
  checkPartial("'\\4", "", "4");
  checkPartial("'\\41", "!", null);

  checkPartial("'\\", "", "");
  checkPartial("'\\1", "", "1");
  checkPartial("'\\17", "", "17");
  checkPartial("'\\172", "z", null);
}

let (checkUneval) {
  checkUneval = function SP_T_CheckUneval(a, b)
  {
    Test.is(StringParser.uneval(a, "'"), b);
  };

  checkUneval("omghax", "'omghax'");
  checkUneval("'Hello.'", "'\\'Hello.\\''");
  checkUneval("'\\'Hello.\\''", "'\\'\\\\\\'Hello.\\\\\\'\\''");

  checkUneval("\n", "'\\n'");
  checkUneval("\r", "'\\r'");

  checkUneval = function SP_T_CheckUneval(a, b)
  {
    Test.is(StringParser.uneval(a, "\""), b);
  };

  checkUneval("omghax", "\"omghax\"");
  checkUneval("\"Hello.\"", "\"\\\"Hello.\\\"\"");

  // Test that quoteType defaulting behavior is correct for both missing and
  // explicitly undefined.
  Test.is(StringParser.uneval("hax"), "\"hax\"");
  Test.is(StringParser.uneval("hax", undefined), "\"hax\"");

  // XXX Moar tests, especially aUnescapeSequences
}

let (parser = new StringParser("'\\x")) {
  let exception = null;
  try {
    parser.value;
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  parser.feed("0");
  exception = null;
  try {
    parser.value;
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
}

let (parser = new StringParser("'\\u")) {
  let exception = null;
  try {
    parser.value;
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  parser.feed("0");
  exception = null;
  try {
    parser.value;
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  parser.feed("0");
  exception = null;
  try {
    parser.value;
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  parser.feed("0");
  exception = null;
  try {
    parser.value;
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
}

let (parser = new StringParser("'\\x")) {
  let exception = null;
  try {
    parser.feed("g");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
  Test.is(parser.state, StringParser.DEAD_STATE);

  parser = new StringParser("'\\x0");
  exception = null;
  try {
    parser.feed("g");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
  Test.is(parser.state, StringParser.DEAD_STATE);
}

let (parser = new StringParser("'\\u")) {
  let exception = null;
  try {
    parser.feed("g");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
  Test.is(parser.state, StringParser.DEAD_STATE);

  parser = new StringParser("'\\u000");
  exception = null;
  try {
    parser.feed("g");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
  Test.is(parser.state, StringParser.DEAD_STATE);
}

let (parser) {
  let exception = null;
  try {
    parser = new StringParser("'\\x\\7a'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  exception = null;
  try {
    parser = new StringParser("'\\x7\\a'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
}

let (parser) {
  let exception = null;
  try {
    parser = new StringParser("'\\u\\007a'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  exception = null;
  try {
    parser = new StringParser("'\\u007\\a'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
}

let (parser) {
  let exception = null;
  try {
    parser = new StringParser("'\\x'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  exception = null;
  try {
    parser = new StringParser("'\\x0'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
}

let (parser) {
  let exception = null;
  try {
    parser = new StringParser("'\\u'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  exception = null;
  try {
    parser = new StringParser("'\\u0'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  exception = null;
  try {
    parser = new StringParser("'\\u00'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);

  exception = null;
  try {
    parser = new StringParser("'\\u000'");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
}

let (parser = new StringParser()) {
  let exception = null;
  try {
    parser.feed(""); // Not a character.
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof TypeError);
  Test.is(parser.state, StringParser.DEAD_STATE);

  parser = new StringParser();
  exception = null;
  try {
    parser.feed("also not a character");
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof TypeError);
  Test.is(parser.state, StringParser.DEAD_STATE);

  parser = new StringParser();
  exception = null;
  try {
    parser.feed("n"); // Not a quote.
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof SyntaxError);
  Test.is(parser.state, StringParser.DEAD_STATE);

  exception = null;
  try {
    parser.feed("'"); // It's already dead.
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof Error);
  Test.is(parser.state, StringParser.DEAD_STATE);

  exception = null;
  parser = new StringParser("'a complete literal'");
  try {
    parser.feed(" "); // It's already accepting.
  }
  catch (ex) {
    exception = ex;
  }
  Test.ok(exception != null && exception instanceof Error);
  Test.is(parser.state, StringParser.ACCEPTING_STATE);
}

Test.showResults();
