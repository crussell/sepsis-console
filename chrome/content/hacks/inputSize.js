/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* When entering multiple lines in a console-type textbox, it should
 * automatically resize, and it should all be handled inside the console
 * binding.  Right now it doesn't because I can't think of a clean way to do
 * it.  So it's left up to us as the consumer to handle this ourselves for
 * now.
 *
 * The biggest thing preventing the binding from simply doing what we do here
 * is an opportunistic time to do so.  During XBL construction, things aren't
 * yet in a state where the binding can measure mFluffHeight the way we do
 * here, and I refuse to use setTimeout to do it.  --crussell
 *
 * TODO: Impose some maximum, so that the input can't grow to completely cover
 * the scrollback, or at least not outside the bounds of the window.
 * TODO: It would be nice if we could preserve scrollback position (the way
 * adjustSize does when the input grows and shrinks) on resize, too.  That's
 * probably outside the scope of this hack, though.
 */
function ConsoleInputResizer(aInput, aScrollback)
{
  this.mInput = aInput;
  this.mScrollback = aScrollback;
  this.mConsumeCount = 0;

  // The height of all the widget decorations that surround the text field's
  // content, as measured by scrollHeight.  I.e., the border and padding
  // height of the textbox, plus the margin and border height of
  // this.mInput.inputField, plus the margin, border, and padding height of
  // all intermediate anonymous nodes.
  // XXX bug 755971 changed scrollHeight.
  // <https://staktrace.com/spout/entry.php?id=751>
  this.mFluffHeight =
    this.mInput.boxObject.height - this.mInput.inputField.scrollHeight;

  // The natural height for a 1-row multiline textbox is actually larger than
  // the height needed to show only that row.  (This may have something to do
  // with having space to display scrollbars.)  So force a size adjustment, to
  // give us the appearance we want (i.e., something like a plain old,
  // non-multiline textbox).
  this.adjustInputSize();

  this.mInput.addEventListener("input", this, false);
  // Make sure to also resize when the input is cleared.
  this.mInput.addEventListener("inputready", this, false);
  // And when the window is resized...  Pfft.  What other cases are we
  // missing?  We really probably do want something that works using something
  // related to overflow/underflow events, but those events themselves aren't
  // useful enough.  An "excessunderflow" event would really help us out.
  window.addEventListener("resize", this, false);
};

let (proto = ConsoleInputResizer.prototype) {
  proto.mInput = null;
  proto.mScrollback = null;
  proto.mFluffHeight = null;
  proto.mConsumeCount = null;

  proto.adjustInputSize = function IR_AdjustInputSize()
  {
    // When the input grows or shrinks, and consequently the scrollback
    // respectively shrinks or grows, we want the scrollback contents to
    // maintain their position.  The browser widget does this automatically,
    // but it maintains the scroll position relative to the top of the
    // viewport.  For a console, it makes more sense to maintain this property
    // for whatever's showing at the bottom of the viewport.
    //
    // NB: There's actually a problem even if typing a character results in
    // the input staying the same size.  This occurs because we temporarily
    // set the size of the input to 0 in order to get a useful measurement out
    // of scrollHeight.  When we subsequently set the height to the value we
    // actually want it to be at, if the scrollback was scrolled to a position
    // somewhere near the bottom, the input ends up covering some of what had
    // been visible before we set the height to 0.
    var oldScrollbackX = this.mScrollback.contentWindow.scrollX;
    var oldScrollbackY = this.mScrollback.contentWindow.scrollY;
    var oldScrollbackHeight =
      this.mScrollback.contentDocument.body.clientHeight;

    // Now resize the input to fit its contents.  We initially set the height
    // to 0 so that we can get a useful measurement from the scrollHeight; if
    // the input size is large enough to accommodate its text contents with
    // space left over, scrollHeight gives us a measurement that includes the
    // size of the excess space.  We want something that will tell us the
    // minimum height necessary to accommodate the input's contents.
    // XXX We can get at the anonymous nodes that comprise the input value via
    // the input's editor's selection's range methods.  We could use
    // getClientRects from there to compute the values we need to forego
    // setting the height to 0 beforehand.  But is it worth all that?  Maybe.
    // XXX This could be fragile; mFluffHeight will cause us to break on
    // outside style changes.
    this.mInput.height = 0;
    this.mInput.height =
      this.mFluffHeight + this.mInput.inputField.scrollHeight;

    var newScrollbackHeight =
      this.mScrollback.contentDocument.body.clientHeight;
    var newScrollbackY =
      oldScrollbackY + (oldScrollbackHeight - newScrollbackHeight);
    this.mScrollback.contentWindow.scroll(oldScrollbackX, newScrollbackY);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// implement nsIDOMEventListener

  proto.handleEvent = function IR_HandleEvent(aEvent)
  {
    // adjustInputSize will cause two reflows, where each will fire a resize
    // event.  Handlers which cause the firing of the very events they handle
    // can cause an infinite loop, if not careful.
    //
    // We can't simply bracket the adjustInputSize call with calls to
    // removeEventListener and addEventListener, respectively, to temporarily
    // remove ourselves.  See
    // <https://developer.mozilla.org/en/DOM/element.addEventListener#Adding_a_listener_during_event_dispatch>
    //
    // So we use a counter to mitigate the infinite loopiness.  But the
    // inelegance doesn't stop there!  Due to the way synchronous events are
    // dispatched (the resize event is synchronous), we can't simply bracket
    // the adjustInputSize call with an increment and a decrement to the
    // counter, either, because synchronous events aren't as synchronous as
    // you might think.  See
    // <http://www.w3.org/TR/DOM-Level-3-Events/#sync-async>.
    if (!this.mConsumeCount) {
      this.mConsumeCount += 2;
      this.adjustInputSize();
    }
    else {
      --this.mConsumeCount;
    }
  };
}
