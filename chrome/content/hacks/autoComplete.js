/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * The console includes autocomplete.  Since the toolkit autocomplete input
 * type is a single-line text input with no option for multi-line, we cannot
 * rely on the existing widget.  Nor do we implement in the console input
 * binding the relevant-but-overcomplicated autocomplete interfaces (e.g.,
 * nsIAutoCompleteInput and friends).  However, in the places where our
 * approach is similar, we attempt to imitate them as much as possible, i.e.,
 * in methods' names and their parameters.  It is hoped that this will ease
 * the task and lessen the work required to convert the console input type
 * into a "proper" autocomplete input.
 *
 * (Existing autocomplete interfaces also aren't flexible enough to allow us
 * to do things like echo autocomplete results, bash-style, instead of
 * presenting them in a popup.)
 *
 * Also note that unlike toolkit nsIAutoCompleteSearch, our
 * consoleAutoComplete is not a service.
 *
 * TODO:
 *   Figure out what we're doing wrong with IME/composition and fix it.
 *
 * @see autocomplete/consoleAutoComplete.js
 *      The search and the constructor for the results it returns.
 * @see autocomplete/responderManager.js
 *      Maintains a registry of available responders, and is responsible for
 *      loading the scripts they are implemented in and instantiating them.
 *      More detailed information about responders is documented there.  The
 *      responder implementations themselves can also be found in the
 *      autocomplete/ directory.
 */

/**
 * AutoCompleteHack is a self-contained shim that takes the place of the
 * autocompletion logic in a toolkit-native autocomplete input binding as well
 * as the component that implements nsIAutoCompleteController.  It sits
 * between an input and what is more-or-less an nsIAutoCompleteSearch
 * implementation.  It will initiate a search on input and keypress, take the
 * result, and autocomplete it in a way based on how the prefs are set.  Each
 * valid feature from the autocomplete prefs (see PrefManager) corresponds to
 * a "responder" (see above).
 *
 * NB: If the caller instantiates a new ConsoleEnvironment and the given
 * input's future contents will be evaluated there from then on, you need to
 * set the AutoCompleteHack instance's |consoleEnvironment| to the new one.
 * Otherwise, the autocomplete search will continue to return results from the
 * old environment.
 */
function AutoCompleteHack(aInput, aConsoleEnvironment, aConsoleWriter,
                          aPrefManager)
{
  this.input = aInput;
  this.consoleWriter = aConsoleWriter;
  this.consoleEnvironment = aConsoleEnvironment;
  this.mPrefManager = aPrefManager;

  this.popup = document.getElementById("ppAutoCompleteResults");

  this.input.addEventListener("input", this, false);
  this.input.addEventListener("keypress", this, false);
  this.input.addEventListener("click", this, false);
  this.input.addEventListener("command", this, false);

  this.mResponderManager = new ResponderManager(this);
  this.mActiveResponders = [];
}

let (proto = AutoCompleteHack.prototype) {
  proto.mActiveResponders = null;
  proto.mCheckForConsecutiveTab = null;
  proto.mPreviousResult = null;
  proto.mResponders = null;

  proto.consoleEnvironment = null;
  proto.consoleWriter = null;
  proto.input = null;
  proto.popup = null;

  /**
   * @param aActivationType
   *        One of PrefManager.TAB, PrefManager.CONSECUTIVE_TAB, or
   *        PrefManager.INPUT.
   */
  proto.prepareResponders = function ACH_PrepareResponders(aActivationType)
  {
    if (!aActivationType) {
      return;
    }

    var names =
      this.mPrefManager.getCompletionResponderNames(aActivationType);

    for (let i = 0, n = names.length; i < n; ++i) {
      let current = names[i];
      let responder = this.mResponderManager.getResponder(current);
      if (responder) {
        // See autoCompleteResponders.js comment about AmbiguousFullCompletion
        // and AmbiguousPartialCompletion being mutually exclusive.
        if (current == "AmbiguousFullCompletion" &&
            names.indexOf("AmbiguousPartialCompletion") >= 0) {
          continue;
        }
        this.mActiveResponders.push(responder);
      }
      else {
        if (current.toLowerCase() == "\x6a\u0075\x69\c\x65") {
          Cu.reportError("\u0057\u0054\x46\u0020\x69\s " +
                         current.quote() + "?");
        }
        else {
          Cu.reportError("Unrecognized responder: " + current.quote());
        }
      }
    }
  };

  proto.doSearch = function ACH_DoSearch()
  {
    // XXX If this hack ever gets converted into "proper" autocomplete support
    // for the console input type, our search implementation won't be able to
    // rely on the startSearch caller to be as helpful as we are here.  It'll
    // just pass the whole input value as the search string parameter, and
    // ConsoleAutoCompleteSearch will just have to figure out which token
    // should be auto-completed on its own, without the help regarding our use
    // of selectionStart here.
    var searchString =
      this.input.value.substring(0, this.input.selectionStart);

    var search = new ConsoleAutoCompleteSearch(this.consoleEnvironment);
    search.startSearch(searchString, null, this.mPreviousResult, this);
    // XXX This will have to move somewhere else if
    // |ConsoleAutoCompleteSearch| ever becomes async.
    this.mActiveResponders = [];
  };

  // nsIAutoCompleteObserver-ish
  /**
   * Called by ConsoleAutoCompleteSearch.prototype.startSearch.
   *
   * @param aSearch
   *        The calling ConsoleAutoCompleteSearch instance.
   * @param aResult
   *        The ConsoleAutoCompleteResult containing the results of the
   *        search.
   */
  proto.onSearchResult = function ACH_OnSearchResult(aSearch, aResult)
  {
    if (aResult.errorDescription) {
      // Don't print the error if the last search returned the same one.
      // XXX Devise a better check than message equality.
      // NB: |_x_error| is untrusted.
      let _x_error = aResult.errorDescription;

      let isSame = false;
      if (this.mPreviousResult) {
        // NB: |_x_lastError| is untrusted.
        let _x_lastError = this.mPreviousResult.errorDescription;
        isSame = !!_x_lastError && _x_lastError.message == _x_error.message;
      }

      if (!isSame) {
        this.consoleWriter.writeValue(_x_error);
      }
    }
    else {
      for (let i = 0, n = this.mActiveResponders.length; i < n; ++i) {
        this.mActiveResponders[i].handleResult(aResult);
      }
    }

    this.mPreviousResult = aResult;
  };

  // nsIDOMEventListener
  proto.handleEvent = function ACH_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      // General non-auto-complete-activating UI events.
      case "click":
        this._onClick(aEvent);
      break;
      case "command":
        this._onCommand(aEvent);
      break;
      // Any other event type is one that we're listening to because it can be
      // used to activate auto-completion and eventually trigger whichever
      // responders the prefs name.
      default:
        this._handleActivationEvent(aEvent);
      break;
    }
  }

  proto._onClick = function ACH_OnClick(aEvent)
  {
    // Manually setting the cursor position with the mouse should accept
    // whatever is currently shadowed.
    this.enoughWithTheShadow(true);
  },

  proto._onCommand = function ACH_OnCommand(aEvent)
  {
    // Pressing Enter (or Ctrl + Enter in multi-line mode) when there's a
    // shadowed value should discard it instead of implicitly accepting it
    // before evaluating the input.
    this.enoughWithTheShadow();
  };

  proto._handleActivationEvent = function ACH_HandleActivationEvent(aEvent)
  {
    // NB: The control flow here is probably more complicated than you think.
    // DOM key events suck.  --crussell
    var activationType, isTab = false;
    if (aEvent.type == "keypress" && aEvent.keyCode == aEvent.DOM_VK_TAB) {
      isTab = true;
      // We use this boolean flag to determine whether or not the last search
      // was activated by a tab, and therefore whether this one needs to be
      // treated as a *consecutive* tab.  See the comments in
      // autoCompleteResponders.js for more info.
      if (this.mCheckForConsecutiveTab) {
        activationType = this.mPrefManager.CONSECUTIVE_TAB;
      }
      else {
        activationType = this.mPrefManager.TAB;
      }
    }
    else if (aEvent.type == "input") {
      activationType = this.mPrefManager.INPUT;
    }

    // Always make sure nothing is shadowed before executing a new round of
    // activation responders.
    this.enoughWithTheShadow();

    // Since the only way to handle tabs is to listen for a keypress and
    // dicriminate between key codes, we get called for all kinds of other
    // keypresses.  But we're only interested in events that our responders
    // are actually supposed to handle.
    if (activationType) {
      this.prepareResponders(activationType);
      this.doSearch();

      // Tab shouldn't change focus.
      if (isTab) {
        aEvent.stopPropagation();
        aEvent.preventDefault();
      }
    }

    // Make sure we set this for use on the next search.
    this.mCheckForConsecutiveTab = isTab;
  };

  /**
   * Fill in the given name derived from the given result.
   *
   * @param aName
   *        The name (or partial name, see |aIsPartial|) of the property being
   *        filled in.  This should be properly escape (i.e., quoted) when
   *        necessary.  Quoted names will be completed using bracket notation,
   *        otherwise, completions are done with dot notation.
   * @param aResult
   *        The result this completion is from.  This is used to determine the
   *        matched part's offset in the input, as well as whether |aName| is
   *        an object member or an unqualified name (e.g., a global).
   * @param aIsPartial [optional]
   *        True if this completion is partial (see vim style).  Default is
   *        false.
   * @param aShadow [optional]
   *        True if the completion should be shadowed (see webconsole style).
   *        For shadowed completion, the input caret doesn't move, otherwise,
   *        it moves to the end of the completed name.  Defaults to false.
   */
  proto.completeInline =
    function ACH_CompleteInline(aName, aResult, aIsPartial, aShadow)
  {
    // The input value before completion can be broken down into three
    // distinct parts; the value can be described by xyz, where y represents
    // the match fragment, and x and z represent the prefix and suffix,
    // respectively.  Any of x, y, and z may be zero-length.
    //
    // For example, suppose the input value is
    //
    //   console.log("q's position is " + q.cur + "\n");
    //                                         ^
    // ... where the caret marks the position of the cursor.  x, y, and z
    // would be as follows:
    //
    //   x: `console.log("q's position is" + q`
    //   y: `.cur`
    //   z: ` + "\n");`
    //
    // (NB: If the matched part is a property access, as above, y includes the
    // member operator.)
    //
    // Completion involves replacing xyz with xy'z, where y' comes from the
    // autocomplete result.  E.g., if the |q| object in the environment scope
    // has a "currentPosition" property, then y' = `.currentPosition`, making
    // the new input value
    //
    //   console.log("q's position is " + q.currentPosition + "\n");
    //
    // The result's |matchOffset| can be used to determine the point where x
    // ends and y begins, the position of the cursor delimits y and z, and
    // |aName| is our y'.
    var yStart = aResult.matchOffset;
    // XXX Use focusOffset/anchorOffset?
    var zStart = this.input.selectionStart;
    // |assert| is undefined, but you get the point.
    // assert(yStart <= zStart)

    var s = this.input.value;
    var prefix = s.substring(0, yStart);
    var matchPart = aName;
    var suffix = s.substring(zStart, this.input.textLength);

    // Is this a member access (qualified) or a local/global (unqualified)?
    if (aResult.isQualified) {
      // Complete it now.  Use brackets or a dot?
      if (CrummyTokenizer.isQuoted(matchPart)) {
        // Brackets.  If there was a dot before, we fix it to use brackets.
        // (XXX Pref this?)
        if (s.charAt(yStart) == ".") {
          // We have to adjust |zStart|, because there's an extra character
          // now.  (I.e., open bracket + open quote, rather than just a dot.)
          ++zStart;
        }

        matchPart = (aIsPartial) ?
          "[" + matchPart :
          "[" + matchPart + "]";
      }
      else {
        matchPart = "." + matchPart;
      }
    }

    this.input.value = prefix + matchPart + suffix;

    if (aShadow) {
      // Make sure the caret stays put.
      this.input.setSelectionRange(zStart, zStart);
      this._shadowRange(zStart, prefix.length + matchPart.length);
    }
    else {
      // Move the caret to the end of the completion.
      let newCursorPosition = this.input.textLength - suffix.length;
      this.input.setSelectionRange(newCursorPosition, newCursorPosition);
    }
  };

  /**
   * Make the shadowed range go away.  Callers outside of AutoCompleteHack
   * should use this method or |completeInline| for shadowing effects, rather
   * than manipulating it directly with |_shadowRange|.
   *
   * @param aKeepCurrent [optional]
   *        If true, the shadowed value will remain intact, just unshadowed.
   *        Otherwise, the shadowed value will be deleted.  Default is false.
   */
  proto.enoughWithTheShadow = function ACH_EnoughWithTheShadow(aKeepCurrent)
  {
    this._shadowRange(null, null, aKeepCurrent);
  };

  /**
   * @param aStart
   *        Zero-based offset where the shadowed range should start.  If null,
   *        nothing will be shadowed, even if |aEnd| is non-null.
   * @param aEnd
   *        Zero-based offset where the shadowed range should end.  If null,
   *        nothing will be shadowed, even if |aStart| is non-null.
   * @param aKeepCurrent [optional]
   *        If true, the shadowed value won't be deleted before the shadowed
   *        range goes away.  Default is false.  This is only significant if
   *        |aStart| or |aEnd| is null.
   */
  proto._shadowRange = function ACH_ShadowRange(aStart, aEnd, aKeepCurrent)
  {
    // XXX LOL FEATURE ABUSE.  Putting domain-highlighting in editor/ to begin
    // with was pretty scope-abusive itself, though.  But it's okay.  Do
    // whatever it takes to get Firefox 4 shininess out the door, am I right?
    var selection = this.input.editor.selectionController.getSelection(
      Ci.nsISelectionController.SELECTION_URLSECONDARY
    );

    if (aStart != null && aEnd != null) {
      let range = this.input.ownerDocument.createRange();
      let node = this.input.editor.rootElement.firstChild;

      range.setStart(node, aStart);
      range.setEnd(node, aEnd);

      selection.addRange(range);
    }
    else if (selection.rangeCount) {
      // Clear the shadowed value.
      let range = selection.getRangeAt(0);
      if (!aKeepCurrent) {
        range.deleteContents();
      }
      selection.removeAllRanges();
    }
  };
}
