/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * XXX Pfft.  This is so wrong.  Really, unless we know otherwise, (e.g.,
 * through some magical isFastAndSideEffectFree method like jorendorff
 * mentions) all getters should be treated as unsafe.  Taking that path,
 * though, means that autocomplete is significantly less useful for DOM
 * objects.  The devtools Web Console seems to take the position that native
 * getters are okay, and we're shooting for parity there, so we'll go with
 * that for now, as bad of an assumption as that is.
 */
var MemberAccessTrustHack = {
  /**
   * @param aObj
   *        The base object.
   * @param aID
   *        The name of the property on |aObj| for which we are trying to
   *        determine whether we can trust accessing it.
   * @return True if it's okay to access |aObj[aID]|, undefined if the object
   *         has no such property, and false otherwise.
   */
  isMemberAccessTrustable:
    function MATH_IsMemberAccessTrustable(aObj, aID)
  {
    // Call __lookupGetter__ before Object.getOwnPropertyDescriptor, because
    // the latter doesn't always reveal getters/setters for some native
    // objects until the former has been called.  See bug 784747.
    Object.prototype.__lookupGetter__.call(aObj, aID);
    // XXX Need to guard against getPrototypeOf for proxies.  (That's only for
    // the old Proxy API, i.e., pre-direct proxies; pre-Gecko 18.)
    for (let obj = aObj; obj; obj = Object.getPrototypeOf(obj)) {
      // XXX Need to guard against proxy traps for this method.
      let descriptor = Object.getOwnPropertyDescriptor(obj, aID);
      if (descriptor) {
        // XXX Being paranoid about getOwnPropertyDescriptor traps means not
        // assuming that |descriptor| itself isn't a proxy or that
        // |descriptor.get| isn't itself a getter, either.  Jeez.
        if ("get" in descriptor) {
          // XXX The following check is ridiculous.  It's what the Web Console
          // does, though, and it interprets prototype-less functions to mean
          // "native".  We *should* be guarding against functions returned by
          // Function.prototype.bind and function proxies created with a null
          // prototype.
          return !("prototype" in descriptor.get);
        }

        return true;
      }
    }

    return undefined;
  }
};
