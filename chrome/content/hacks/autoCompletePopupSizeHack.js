/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Compute and set a reasonable size for the autocomplete results popup.
 *
 * I tried starting with the popup's natural width and using a base-2
 * exponential blow up to increase its size until we can accommodate the row
 * with the greatest necessary width, but I couldn't get any useful metrics
 * out of the tree box object methods doing that.  With every increase, they
 * always yielded the same values as the first time they were called.
 *
 * So we just start the popup at the maximum width we would let it get to, and
 * just pare it down--if we can--to something more appropriately smaller,
 * right after the popup first appears.  This causes jank, but this is already
 * a hack, and I've already spent too much time on this for now. --crussell
 */

function AutoCompletePopupSizeHack(aPopup, aTree, aResult)
{
  this.mPopup = aPopup;
  this.mTree = aTree;
  this.mResult = aResult;

  this.mLongestIndex = -1;
  for (let i = 0, n = this.mResult.matchCount; i < n; ++i) {
    if (this.mLongestIndex < 0 ||
        this.mResult.getValueAt(this.mLongestIndex).length <
          this.mResult.getValueAt(i).length) {
      this.mLongestIndex = i;
    }
  }

  if (this.mPopup.popupBoxObject.popupState == "open") {
    // Don't reduce the width of existing popups, but *do* increase if
    // necessary.  The use case is this:
    // - Create an object "fum" with only properties "x1", "x2", and
    //   "omghaxwtf".
    // - Type |fum.x1| into the input.
    // - Make sure AmbiguousPopup is turned on for the input activation type.
    // - Hit backspace so that the input value is |fum.x| and the "x1" and
    //   "x2" properties are showing in the popup.
    // - Now hit backspace again.  All of the properties should be listed in
    //   the popup, and it should have grown wide enough to accommodate
    //   "omghaxwtf".
    //
    // So we want it to increase in size there.  But now do this:
    // - Type |fum.| into the input.  All its properties should be showing.
    // - Press "x" so that the input shows |fum.x| and the "x1" and "x2"
    //   properties are listed.
    //
    // In that case, we don't want the popup to shrink; the width after the
    // second step should be the same as the width after the first one.
    let column = this._getColumn();
    let adjustWidth =
      aTree.treeBoxObject.isCellCropped(this.mLongestIndex, column) &&
      aPopup.width != this.MAX_WIDTH;

    if (adjustWidth) {
      aPopup.width = this.MAX_WIDTH;
    }

    this._adjustSize(!adjustWidth);
    return;
  }

  this.mPopup.width = this.MAX_WIDTH;

  this.mPopup.addEventListener("popupshown", this, false);
  this.mPopup.addEventListener("popuphiding", this, false);
}


let (proto = AutoCompletePopupSizeHack.prototype) {
  proto.mPopup = null;
  proto.mResult = null;
  proto.mTree = null;

  // XXX Magic numbers.
  proto.MAX_WIDTH = AutoCompletePopupSizeHack.MAX_WIDTH = 512;
  proto.MAX_ROWS = AutoCompletePopupSizeHack.MAX_ROWS = 9;

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function ACPSH_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "popupshown":
        this._adjustSize();
      break;
      case "popuphiding":
        this._onPopupHiding();
      break;
      default:
        Cu.reportError("wat.  event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._getColumn = function ACPSH_GetColumn()
  {
    return this.mTree.treeBoxObject.columns.getNamedColumn(
      "colAutoCompleteValue"
    );
  };

  /**
   * @param aHeightOnly
   *        Adjust only the height, not the width.
   */
  proto._adjustSize = function ACPSH_AdjustSize(aHeightOnly)
  {
    let popupRect = this.mPopup.getBoundingClientRect();
    let treeBodyRect = this.mTree.body.getBoundingClientRect();

    // Compute the height.  Use the same algorithm as that used in toolkit's
    // autocomplete.xml.
    var rows = this.MAX_ROWS;
    if (this.mResult.matchCount < rows) {
      rows = this.mResult.matchCount;
    }
 
    var height = rows * this.mTree.treeBoxObject.rowHeight;
    if (height) {
      height += popupRect.height - treeBodyRect.height;
      this.mTree.setAttribute("height", height);
    }
    else {
      this.mPopup.hidePopup();
    }
 
    // Now do the width.
    if (aHeightOnly) {
      // . . . or don't.
      return;
    }

    let charWidth = this._computeCharacterWidth();
    if (!charWidth) {
      // assert(this.mPopup.width = this.MAX_WIDTH);
      // Every row was cropped.  There's nothing more we can do.
      return;
    }

    let xOut = {};
    this.mTree.treeBoxObject.getCoordsForCellItem(
      this.mLongestIndex, this._getColumn(), "text", xOut, {}, {}, {}
    );

    // XXX Magic number
    let computedWidth =
      charWidth * this.mResult.getValueAt(this.mLongestIndex).length +
      xOut.value + popupRect.width - treeBodyRect.width + 8;

    if (computedWidth < this.MAX_WIDTH) {
      this.mPopup.width = computedWidth;
    }
  };

  proto._onPopupHiding = function ACPSH_OnPopupHiding()
  {
    this.mPopup.removeEventListener("popupshown", this, false);
    this.mPopup.removeEventListener("popuphiding", this, false);
  };

  /**
   * Assumes text is fixed width.  Otherwise, this is not so useful.
   *
   * @return
   *        The ratio of an arbitrary row's text cell item's width to the
   *        length of the string displayed there, or 0 if all rows are
   *        cropped.
   */
  proto._computeCharacterWidth = function ACPSH_ComputeCharacterWidth()
  {
    for (let i = 0, n = this.mResult.matchCount; i < n; ++i) {
      if (!this.mTree.treeBoxObject.isCellCropped(i, this._getColumn())) {
        let widthOut = {};
        this.mTree.treeBoxObject.getCoordsForCellItem(
          i, this._getColumn(), "text", {}, {}, widthOut, {}
        );
        return Math.ceil(widthOut.value / this.mResult.getValueAt(i).length);
      }
    }

    return 0;
  };
}
