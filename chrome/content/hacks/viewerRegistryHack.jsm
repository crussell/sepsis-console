/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * The inspector's |ViewerManager| got renamed to |ViewerRegistry|.
 *
 * XXX Remove this when we drop support for sepsis-inspector versions < 0.4.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "ViewerRegistry" ];

const Cu = Components.utils;

const oldURI = "chrome://sepsis-inspector/content/utils/viewerManager.jsm";
const newURI =
  "chrome://sepsis-inspector/content/registration/viewerRegistry.jsm";

//////////////////////////////////////////////////////////////////////////////

let (scope = Object.create(null)) {
  try {
    Cu.import(newURI, scope);
    ViewerRegistry = scope.ViewerRegistry;
  }
  catch (ex) {
    Cu.import(oldURI, scope);
    ViewerRegistry = scope.ViewerManager;
  }
}
